#!/bin/bash
echo "Pull Docker image using credentials..."
docker login -u spotdyna -p spotdyna
for file in /home/r/release150/microservicios-master/deploykubernetes/deployments/dev/*.yaml; do
  image=`cat ${file} | grep -i 'image:\\s*spotdyna' | awk '{print \$(NF)}'`
  if [[ "$image" != "" ]]; then
    echo "docker pull $image"
  fi
done
#for file in cronjobs/dev/*.yaml; do
#  image=`cat \$file | grep -i 'image:\\s*spotdyna' | awk '{print \$(NF)}'`
#    if [[ "\$image" != "" ]]; then
#      echo 'docker pull \$image'
#    fi
#done
echo "Todas las imágenes han sido descargadas correctamente..."
