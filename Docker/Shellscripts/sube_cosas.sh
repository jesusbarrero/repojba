echo "Push Docker images to QA account"
docker logout
docker login -u spotdynaqa -p spotdynaqa
for file in /home/r/release150/microservicios-master/deploykubernetes/deployments/cert/*.yaml; do
  image=`cat $file | grep -i 'image:\\s*spotdyna' | awk '{print \$(NF)}'`
  if [ "${image}" != "" ]; then
    imageBasename=`basename ${image} | tr -d '\r'`
    docker push spotdynaqa/$imageBasename
  fi
done
echo "Todas las imágenes se han publicado en la cuenta de QA..."
