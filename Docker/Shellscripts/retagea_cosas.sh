echo "Tag Docker image"
for file in /home/r/release150/microservicios-master/deploykubernetes/deployments/dev/*.yaml; do
  oldImage=`cat $file | grep -i 'image:\\s*spotdyna' | awk '{print \$(NF)}'`
  if [ "$oldImage" != "" ]; then
    imageBasename=`basename \$oldImage`
      docker tag $oldImage spotdynaqa/${imageBasename}
  fi
done
#for file in cronjobs/dev/*.yaml; do
#  oldImage=`cat \$file | grep -i 'image:\\s*spotdyna' | awk '{print \$(NF)}'`
#  if [[ "$oldImage" != "" ]]; then
#    imageBasename=`basename \$oldImage`
#    docker tag $oldImage spotdynaqa/\$imageBasename
#  fi
#done
echo "Todas las imágenes se han renombrado a la cuenta de QA..."
