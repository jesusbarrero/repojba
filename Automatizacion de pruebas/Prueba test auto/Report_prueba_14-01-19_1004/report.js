$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/beacons.feature");
formatter.feature({
  "line": 1,
  "name": "Añadir contenido Beacons",
  "description": "",
  "id": "añadir-contenido-beacons",
  "keyword": "Feature"
});
formatter.before({
  "duration": 179389,
  "status": "passed"
});
formatter.before({
  "duration": 66044,
  "status": "passed"
});
formatter.before({
  "duration": 100851,
  "status": "passed"
});
formatter.before({
  "duration": 65151,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Añadir contenido",
  "description": "",
  "id": "añadir-contenido-beacons;añadir-contenido",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "un beacon provisionado",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "se incluye contenido de tipo texto con descripcion textoDesdeEclipse",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "el contenido se asocia correctamente",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "Consultamos el contenido",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "listamos contenidos del usuario",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Creamos una playlist",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Leemos contenido",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Borramos la playlist-contenido",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Borramos el contenido",
  "keyword": "And "
});
formatter.match({
  "location": "Beacons.provisionBeacon()"
});
formatter.result({
  "duration": 53173798282,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "texto",
      "offset": 29
    },
    {
      "val": "textoDesdeEclipse",
      "offset": 51
    }
  ],
  "location": "Beacons.incluirContenido(String,String)"
});
formatter.result({
  "duration": 9835169842,
  "status": "passed"
});
formatter.match({
  "location": "Beacons.checkContenido()"
});
formatter.result({
  "duration": 2260457568,
  "status": "passed"
});
formatter.match({
  "location": "Beacons.consultamos_el_contenido()"
});
formatter.result({
  "duration": 3803786987,
  "status": "passed"
});
formatter.match({
  "location": "Beacons.listamos_el_contenido()"
});
formatter.result({
  "duration": 3593913171,
  "status": "passed"
});
formatter.match({
  "location": "Beacons.creamos_una_playlist()"
});
formatter.result({
  "duration": 3787369748,
  "status": "passed"
});
formatter.match({
  "location": "Beacons.Leemos_un_contenido()"
});
formatter.result({
  "duration": 3951750094,
  "status": "passed"
});
formatter.match({
  "location": "Beacons.Borramos_la_playlist()"
});
formatter.result({
  "duration": 1413669371,
  "status": "passed"
});
formatter.match({
  "location": "Beacons.borramos_el_contenido()"
});
formatter.result({
  "duration": 13026987818,
  "status": "passed"
});
});