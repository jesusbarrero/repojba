#!/bin/bash

PASSWORD='11f00190c6fa688437b9f808d076d356bb'
USER='laboratorio'
SERVER="http://192.168.84.82:8081"
TOKEN='prueba'

#jobs QA
MKDANDROIDJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-MKDAndroid-qarelease'
MKDWINDOWSJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-MKDWindows-qarelease'
HANJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-HAN-qarelease'
PROVISIONJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-Spotdyna-Tests-Provision-qarelease'
BAJASJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-Spotdyna-Tests-Bajas-Sustitucion-qarelease'
TAGSJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-Tags-qarelease'
MICROSERVICIOSJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-Spotdyna-Tests-Microservicios-qarelease'
TRAMASMKSJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-TramasMKD-qarelease'
MIGRACIONJOBQA='job/Features_Tests/view/QA-qarelease/job/OTS-Migracion-qa-release/'

#jobs DEV
MKDWINDOWSJOBDEV='job/Features_Tests/view/Dev/job/OTS-MKDWindows-dev'
HANJOBDEV='job/Features_Tests/view/Dev/job/OTS-HAN-dev'
MICROSERVICIOSJOBDEV='job/Features_Tests/view/Dev/job/OTS-Spotdyna-Tests-Microservicios-dev'

#job PRECERT
GENERICTESTJOB='/job/Features_Tests/view/Generic-Test/job/OTS-Cucumber-JVM-Generic-Test'

#job Cypress
CYPRESSJOB='job/Cyperss_Tests/job/Cypress-Spotdyna'

scheduleBackendTest(){
    echo "Ejecutando el test ${JOB}"
    curl -i -X POST  "${SERVER}/${JOB}/buildWithParameters?token=${TOKEN}&delay=300sec" -H "Jenkins-Crumb:$CRUMBS" --user ${USER}:${PASSWORD}
}

scheduleCypressTest(){
    echo "Ejecutando el test ${TEST} de Cypress sobre el entorno $ENTORNO"
    curl -i -X POST  "${SERVER}/${CYPRESSJOB}/buildWithParameters?token=${TOKEN}&testSuite=${TEST}&entorno=${ENTORNO}" -H "Jenkins-Crumb:$CRUMBS" --user ${USER}:${PASSWORD}
}

scheduleGenericTest(){
    echo "Ejecutando el test ${TEST} de Cucumber-JVM sobre el entorno $ENTORNO"
    curl -i -X POST  "${SERVER}/${JOB}/buildWithParameters?token=${TOKEN}&testSuite=${TEST}&entorno=${ENTORNO}" -H "Jenkins-Crumb:$CRUMBS" --user ${USER}:${PASSWORD}
}

#jenkins job parameters

#PARAMF=$1
#SECONDPARAM=$2

# retrieve the crumb that we need to pass in the header

CRUMBS=$(curl -s -X GET -u $USER:$PASSWORD ${SERVER}/crumbIssuer/api/json  | jq -c '. | .crumb ')
#echo $CRUMBS
#curl --user $USER:$PASSWORD  -H "Jenkins-Crumb:${CRUMBS}" -X POST  "${SERVER}/${MKDANDROID_JOB}/buildWithParameters?TOKEN=${TOKEN}&PARAMETERONE=${PARAMF}&PARAMETERTWO=${SECONDPARAM}"
echo "Script para ejecución de los test automáticos de Spotdyna"
echo "Selecciona el entorno"

echo "1) CERT"
echo "2) DEV"
echo "3) PRECERT"

read opcion

echo "Tipo de test"

echo "A) Frontend"
echo "B) Backend"
echo "C) Frontend y Backend"

read tipoTest

case $opcion in
    1)
        ENTORNO='cert'
        if [[($tipoTest == B) || ($tipoTest == C)]]; then
        #Tests de backend
            for JOB in $MKDWINDOWSJOBQA $HANJOBQA $PROVISIONJOBQA $TAGSJOBQA $MICROSERVICIOSJOBQA $MIGRACIONJOBQA
            do
            scheduleBackendTest
            done
        fi
        if [[($tipoTest == A) || ($tipoTest == C)]]; then    
        #Tests de Cypress
            for TEST in 'ConfiguracionGeneral-CrearConfiguracion' 'ConfiguracionGeneral-Detalle' 'ConfiguracionGeneral-Listado' 'ConfiguracionPermisos' 'ConfiguracionRoles' 'ConfiguracionUsuarios-Listado' 'ContenidosParrillas-Listado' 'ContenidosParrillas-Tags'
            do
            scheduleCypressTest  
            done
        fi    
            ;;
    2)
        ENTORNO='dev'
        if [[($tipoTest == B) || ($tipoTest == C)]]; then
        #Tests de backend
            JOB=$GENERICTESTJOB
            for TEST in 'MKD' 'HAN' 'MKDAndroid' 'Provision' 'Migracion'
            do
            scheduleGenericTest
            done
        fi 
        if [[($tipoTest == A) || ($tipoTest == C)]]; then   
        #Tests de Cypress
            for TEST in 'ConfiguracionGeneral-CrearConfiguracion' 'ConfiguracionGeneral-Detalle' 'ConfiguracionGeneral-Listado' 'ConfiguracionPermisos' 'ConfiguracionRoles' 'ConfiguracionUsuarios-Listado' 'ContenidosParrillas-Listado' 'ContenidosParrillas-Tags'
            do
            scheduleCypressTest  
            done
        fi    
        ;;
    3)
        ENTORNO='precert'
        if [[($tipoTest == B) || ($tipoTest == C)]]; then
        #Tests de backend
            JOB=$GENERICTESTJOB
            for TEST in 'MKD' 'HAN' 'MKDAndroid' 'Provision'
            do
            scheduleGenericTest
            done
        fi    
        ;;
    *) 
        echo "Opcion incorrecta"
        ;;
esac