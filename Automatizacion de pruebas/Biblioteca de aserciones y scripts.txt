BIBLIOTECA DE ASERCIONES Y SCRIPTS EN GROOVY PARA SOAPUI

*****************************************************

- Obtiene el valor de la propiedad de otro TestStep

 testRunner.testCase.getTestStepByName["BuscaAdjunto"].getPropertyValue("adjunto")
 
*****************************************************

- Inicializa variables

testRunner.testCase.setPropertyValue("resutado","")

*****************************************************

- Coge el valor de una propiedad del testCase actual

 testRunner.testCase.getPropertyValue("data")
 
*****************************************************

OBTENCION DE LA RESPUESTA DE UNA PETICION HTTP

import java.util.regex.Pattern
import java.util.regex.Matcher

def headers = messageExchange.response.responseHeaders
if (headers['#status#'] != null){
	def actualHTTPResponse = headers['#status#'].get(0)
	def m = actualHTTPResponse =~ "\\S+\\s(\\d+)\\s.+"
	context.testCase.setPropertyValue("responseCode", m[0][1])	
}

*****************************************************

- Comprueba el HTTP Status del testStep "api-playlist-consulta" y lo devuelve en la propiedad "resultado"

import groovy.json.JsonSlurper

def httpResponseHeaders = context.testCase.testSteps["api-playlist-consulta"].testRequest.response.responseHeaders
def httpStatus = httpResponseHeaders["#status#"]
def httpStatusCode = (httpStatus =~ "[1-5]\\d\\d")[0]
context.testCase.setPropertyValue("resultado", httpStatusCode)

*****************************************************

- Lee un campo del JSON, si no lo encuentra devuelve un valor ""

import groovy.json.JsonSlurper

def response = new JsonSlurper().parseText(messageExchange.response.responseContent)
	context.testCase.setPropertyValue("data", response.data.toString())


*****************************************************

- Script que compara si String1 contiene String2

import static org.junit.Assert.assertThat
import static org.hamcrest.CoreMatchers.containsString

def String1 = testRunner.testCase.getPropertyValue("String1")
def String2 = testRunner.testCase.getPropertyValue("String2")

def resultado =  String1.contains(String2)
assert resultado

*****************************************************

- Script que recorre el array response

import groovy.json.JsonSlurper

def responseText = messageExchange.response.responseContent
def response = new JsonSlurper().parseText(responseText)
def result = ""

def currentPosition = 0
def firstPlaylist = true
while (response!=null && response[currentPosition]!=null){
	if (firstPlaylist)
		firstPlaylist = false
	else
		result += ","
	result += response[currentPosition].IDPLAYLIST
	currentPosition++
}

context.testCase.setPropertyValue("idPlaylistsRecuperadas", result)


****************************************************

VERIFICAR QUE HAY AL MENOS UN CAMPO DE TIPO PLAYLIST_DISPOSITIVO.ID EN UNA QUERY SQL

import groovy.util.*
import groovy.lang.*
import com.eviware.soapui.model.testsuite.*

def groovyUtils = new com.eviware.soapui.support.GroovyUtils(context)
def responseHolder = groovyUtils.getXmlHolder(context.responseAsXml)
def resultsNumber = responseHolder.getNodeValues("//*:Results/*:ResultSet/*:Row/*:PLAYLIST_DISPOSITIVO.ID").length
assert resultsNumber == 1

*****************************************************

METODO PARA OBTENER FECHA Y HORA

 private String getFechaHora(){
    	return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
   }
   
https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html#patterns

*****************************************************

EJECUTAR UN COMANDO DESDE CMD

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

def sub="cmd /c tasklist"
Process p=sub.execute()

def out = new StringBuffer()
def err = new StringBuffer()
p.consumeProcessOutput(out,err)
p.waitFor(3, TimeUnit.SECONDS)

return out

*****************************************************

BUSCAR Y ELIMINAR UN PROCESO

Process p = Runtime.getRuntime().exec('tasklist')
BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

while ((s = stdInput.readLine()) != null) {
    		if (s.contains("kubectl")){
    		String[] aux = s.split()
    		Runtime.getRuntime().exec("taskkill /F /PID " + aux[1])
	}
}


*****************************************************

DEFINIR UNA VARIABLE DE ENTORNO

def kubeConfig = "C:\\kubeconfig\\config"
def command = "setx KUBECONFIG " + kubeConfig
Process p = command.execute()
log.info(p.text)

*****************************************************

BUSCAR UN ELEMENTO DENTRO DE UNA COLECCION

def result = includedTags.find { 
	item -> item.id.contains("87988405")
}

http://grails.asia/groovy-find
https://www.tutorialspoint.com/groovy/groovy_find.htm

*****************************************************

RETRODECER A UN PASO ANTERIOR DADO UN TIMEOUT DETERMINADO

Date stopTime = new Date()
context.stopTime = stopTime

def fin = (int)(context.stopTime.getTime())
def inicio = (int)(context.startTime.getTime())

int segundos = (fin - inicio)/1000


try{
	def timeoutSegundos = Integer.parseInt(testRunner.testCase.getPropertyValue("IN_TIMEOUT"))
	def enviado = testRunner.testCase.getTestStepByName("Check comando FICHINI enviado").getPropertyValue("OUT_enviado")
	testRunner.testCase.setPropertyValue("OUT_enviado", enviado)
	if (enviado != "1" && tiempoTranscurrido < timeoutSegundos){
		Thread.sleep(5000)
		testRunner.gotoStepByName("Check comando FICHINI enviado")
	}
} catch (e){
}

*****************************************************

PROCESAMIENTO DE UNA LISTA DE STRING

List<String> usuarios = responseHolder.getNodeValues("//*:Results/*:ResultSet/*:Row/*:USUARIOS_CLIENTES.ID_USUARIO")

def cadena = ""

for (String a: usuarios)
cadena += a + ","

if (usuarios.size() != 0){
cadena = cadena.substring(0,cadena.length()-1) //eliminamos la coma del final
context.testCase.setPropertyValue("usuarios",cadena)
}

*****************************************************

CONVERTIR UN STRING EN UNA LISTA

String num = "22,33,44,55,66,77";
	String str[] = num.split(",");
	List<String> al = new ArrayList<String>();
	al = Arrays.asList(str);
	
******************************************************

CONVERTIR UNA LISTA EN UN STRING CON SEPARADORES

import org.apache.commons.lang.StringUtils;

List<Integer> intList = Arrays.asList(1, 2, 3);
  
StringUtils.join(intList, ","))

Output: 1,2,3

En groovy:

def idDispositivo = StringUtils.join(response.success.idDispositivo,",")

*******************************************************

UTILIZAR EXPRESIONES JSONPATH

import groovy.json.JsonSlurper
import com.jayway.jsonpath.JsonPath

def response = new JsonSlurper().parseText(messageExchange.response.responseContent)

def jPathEx = "*.message" //Obtener el campo message dentro de cualquier nodo hijo

def result = JsonPath.read(response,jPathEx)

- Si usamos el caracter $ hay que escaparlo

JsonPath.read(response,"\$..idTag")

********************************************************

EXTRAER LA PARTE ENTERA DE UN NUMERO DECIMAL

def idCanalWS = (long) (responseHolder.getNodeValue("//*:Results/*:ResultSet/*:Row/*:CANALES_MUSICA.ID_CANAL_WIDESPREAD") as double)

********************************************************

LEER JSON DESDE FICHERO

http://groovy-lang.org/json.html

https://stackoverflow.com/questions/25748170/modifying-json-with-jsonbuilder-in-groovy

import groovy.json.JsonSlurper
 
if (args.size() < 1) {
    println("Missing filename")
    System.exit(1)
}
 
filename = args[0]
 
def jsonSlurper = new JsonSlurper()
data = jsonSlurper.parse(new File(filename))
 
println(data)

********************************************************

IMPRIMIR JSON

import groovy.json.JsonOutput
 
if (args.size() < 1) {
    println("Missing filename")
    System.exit(1)
}
 
filename = args[0]
 
 
def data = [
    name: "Foo Bar",
    year: "2018",
    timestamp: "2018-03-08T00:00:00",
    tags: [ "person", "employee"],
    grade: 3.14
]
 
def json_str = JsonOutput.toJson(data)
def json_beauty = JsonOutput.prettyPrint(json_str)
File file = new File(filename)
file.write(json_beauty)

********************************************************

CREAR Y EDITAR JSON

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

def content = """
{
   "app":{ },
   "at":2,
   "badv":[ ],
   "bcat":[ ],
   "device":{
      "carrier":"310-410",
      "connectiontype":3,
      "devicetype":1,
      "dnt":0,
      "dpidmd5":"268d403db34e32c45869bb1401247af9",
      "dpidsha1":"1234" 
   }
}"""

def slurped = new JsonSlurper().parseText(content)
def builder = new JsonBuilder(slurped)
builder.content.device.dpidsha1 = 'abcd'  
println(builder.toPrettyString())

********************************************************

MOSTRAR EL CONTENIDO DE UN FICHERO


File ficheroPasos = new File("D:/Panel/OntheSpot/Hiptest/piticlin.txt")
println { ficheroPasos.text }

********************************************************

GENERAR UN JSON CONCATENANDO EL CONTENIDO DE VARIOS FICHEROS JSON

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.json.StringEscapeUtils

def jsonSlurper = new JsonSlurper()
def jsonBuilder = new JsonBuilder()

def productPath =  context.testCase.testSuite.project.getPropertyValue("LOCAL_PATH_PRODUCTS")
def productos =  testRunner.testCase.getPropertyValue("productos")

String[] str = productos.split(',');
List<String> listaProductos = new ArrayList<String>();
listaProductos = Arrays.asList(str);

def cadena = ""

Integer i = 1

for (def currentProduct : listaProductos){
currentProduct = jsonSlurper.parse(new File(productPath + currentProduct + ".json"))
//log.info(currentProduct)

jsonBuilder = new JsonBuilder(currentProduct)

// Editamos los campos
currentProduct.idRefProv = testRunner.testCase.getPropertyValue("idContrato")+"_R_000"+i
currentProduct.subscribeDate =  testRunner.testCase.testSteps["GetFecha"].getPropertyValue("fecha")
currentProduct.unSubscribeDate =  ""

cadena = cadena + ",\r\n" + StringEscapeUtils.unescapeJavaScript(jsonBuilder.toPrettyString())

i++
}

return cadena.substring(1,cadena.length())


********************************************************

EJECUTAR CRONJOB KUBERNETES

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;
import java.lang.reflect.Field;

def cronjob = testRunner.testCase.getPropertyValue("cronjob")
def nombreCronjob = testRunner.testCase.getPropertyValue("cronjob") + "-" +  new Date().format('HHmmss')
def namespace = testRunner.testCase.testSuite.project.getPropertyValue("KUBERNETES_NAMESPACE")

command = "kubectl create job --from=cronjob/" + cronjob + "  " + nombreCronjob + " -n " + namespace
Process p = command.execute()

testRunner.testCase.setPropertyValue("nombreCronjob",nombreCronjob)
testRunner.testCase.setPropertyValue("result",p.text)

********************************************************

COMANDO KUBECTL GENERICO

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;
import java.lang.reflect.Field;

def comando = testRunner.testCase.getPropertyValue("comando")
def microservicio = testRunner.testCase.getPropertyValue("microservicio")
def namespace = testRunner.testCase.testSuite.project.getPropertyValue("KUBERNETES_NAMESPACE")

def command = "kubectl get pods -n " + namespace
String podName = ""

Process p = command.execute()
BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()))
def encontrado = false
while ((s = stdInput.readLine()) != null && !encontrado) {
    		if (s.contains(microservicio)){
    		podName = s.split()[0]
    		encontrado = true
	}
}

command = "kubectl  " + comando + " " + podName + " -n " + namespace
p = command.execute()

return(p.text)

********************************************************

Metodo findAll()

http://chuwiki.chuidiang.org/index.php?title=Colecciones_en_Groovy

findAll() devuelve una colección de elementos que cumplan una condición

def a = [0,0,1,2,0,1]

// Devuelve todos los elementos que no sean nulos, 0, false ....
println a.findAll()          // [1, 2, 1]

// Devuelve todos los elementos menores de 2
println a.findAll { element -> element<2}      // [0, 0, 1, 0, 1]

********************************************************

Parsear respuesta de una peticion REST

import groovy.json.JsonSlurper
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.json.StringEscapeUtils

def response = new JsonSlurper().parseText(messageExchange.response.responseContent)

def jsonBuilder = new JsonBuilder(response)
context.testCase.setPropertyValue("response",jsonBuilder.toPrettyString())

********************************************************

Extraer valores de una tabla

xml.'**'.find{it.name() == 'Row'}.childNodes().each{
	def tableName = it.name.split("\\.")[0]+"."
	def varValue=it.text()
	def varName = it.name.replaceAll(tableName,"OUTPUT_")
	context.testCase.setPropertyValue(varName, varValue)	
}

********************************************************
