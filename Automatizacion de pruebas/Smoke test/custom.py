#!/usr/bin/python3
import os
import requests
import signal
import time
import json


def killpf():
    print("")
#   print("#########")
#   print("killing all port-forwards")
    time.sleep(2)
    pid = None
    port = "port"
    for line in os.popen("ps ax | grep " + port + " | grep -v grep"):
        fields = line.split()
        pid = fields[0]
        if pid is not None:
            os.kill(int(pid), signal.SIGKILL)
    time.sleep(5)


def setpf(kubectlpath, namespace, microservice):
    print("")
    print("#########")
    print("Set port-forward " + microservice)
    command = kubectlpath + "kubectl port-forward svc/" + microservice + " -n " + namespace + " 3000:3000 &"
    print("Setting: " + command)
    os.system(command)
    time.sleep(5)


def requestParser(microservicio, response):
    colourOK = '\033[94m'
    colourNOK = '\033[91m'
    colourNO = '\033[0m'
    print(response)
    if '200' in str(response):
        print(colourOK + "microservicio " + microservicio +  " ===> OK" + colourNO)
    else:
        print(colourNOK + "microservicio " + microservicio + " ===> NOK" + colourNO)
