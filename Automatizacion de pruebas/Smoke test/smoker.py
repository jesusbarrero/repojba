#!/usr/bin/python3
import os
import requests
import signal
import time
import json
from custom import killpf, setpf, requestParser

# Basic input configuration
namespace = input('namespace to check: ')      # If you use Python 3
kubectlpath = input('path to EKS kubectl in local pc i.e.: /path/to/file/ : ')

# os.system('export KUBECONFIG=/home/r/EKS/config && export PATH=/home/r/EKS:$PATH')
print("")
print("kubeconfig variable:")
os.system('echo $KUBECONFIG')

print("")
print("#########")
print("Get Kubernetes nodes")
com = kubectlpath + "kubectl get node"
os.system(com)

# Kill all port forwards
print("")
print("Killing all previous port-forwards")
killpf()

# Setting general headers for json requests
headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
}

# Checking microservice jNODE
microservice = "jnode"
# Checking communication jNODE <> CORE
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiI0IiwiaWRfdXN1YXJpbyI6Im51bGwiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjU1MTQzZTk0LWI0MmUtNGNhZS05YTM3LWFiZThiODMzMzEwNiJ9.yQjjtj0k4RmVwv3h5UAzFxANwbzRhnz6cOEEXo5VIQiKV4FNPTBo4KL-SfLSt82tXgNGTJVE6uVZYDiOwzmF8p2J6lgJHJ6VXEJuWYcizBypBng-N0hSp9mkE6S5tyHWRtsc-nT3Zwee9Tjtj8xv4ek7stgIM_ylEdkhWY7EL4gHu9-B6QMMWju15T8XtMGGnbg5KoNvQi-1Cq6h06Wos9XdnfdJ8HZ7g9ICzTly_50ZhUqGX_g_anJy0jdmw2GYNSweMd0T5iMJGKDdwLic_U01FU06U0dg0CvHn5B2v41YO6QA-WLhNz7jC3kf2zGhouyV4-fVpfg9-xWLdXStoA'),
)
print("json to test")
data = '{"strCodServidor": "42"} '
print(data)
# Send request and store response
response = requests.post('http://localhost:3000/api/core/TriggerPull', headers=headers, params=params, data=data)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()


# Checking microservice cuentapersonas
microservice = "cuentapersonas"
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiIxIiwiaWRfdXN1YXJpbyI6Im51bGwiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjAwMzMzNjI0LTU5YWQtNDVkNS04YmUyLWQzNjlkNjVlOTg4YiJ9.ss4bX8DbTBqOHAkdVtztdDjoie91oOmuM5r6flGnSRo6zPtN_zdsldDgyabP7kw7ITqx0Pg7TmSqI_r0pRLxM29R5W96By4SBug464f3HA-GR_r-3WLasguRjC3rq9pkmu_8BLyclNFYTAbSC2NlkygkvjD0NIAAJM9TLnu6P3PcjIj9bFiw-bAYk0_Otx_2hS-x31VSiA5Jq4-WKtK5ADipS3oPsZsg-neQ0Yvd9DfZBRkczI_oJg2fCNIcMtIG1gsz2zrIcJIrvsr3E9XC-cZE83FUnbxuyznEw1Sr2MxGQft-neKsKU7PWPsBfxZSAub2PX6uWpnBEoRolJePtg'),
)
# Send request and store response
response = requests.get('http://localhost:3000/api/ctx-cpersonas-det', headers=headers, params=params)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()


# Checking microservice acciones
microservice = "acciones"
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiIxIiwiaWRfdXN1YXJpbyI6Im51bGwiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjAwMzMzNjI0LTU5YWQtNDVkNS04YmUyLWQzNjlkNjVlOTg4YiJ9.ss4bX8DbTBqOHAkdVtztdDjoie91oOmuM5r6flGnSRo6zPtN_zdsldDgyabP7kw7ITqx0Pg7TmSqI_r0pRLxM29R5W96By4SBug464f3HA-GR_r-3WLasguRjC3rq9pkmu_8BLyclNFYTAbSC2NlkygkvjD0NIAAJM9TLnu6P3PcjIj9bFiw-bAYk0_Otx_2hS-x31VSiA5Jq4-WKtK5ADipS3oPsZsg-neQ0Yvd9DfZBRkczI_oJg2fCNIcMtIG1gsz2zrIcJIrvsr3E9XC-cZE83FUnbxuyznEw1Sr2MxGQft-neKsKU7PWPsBfxZSAub2PX6uWpnBEoRolJePtg'),
)
# Send request and store response
response = requests.get('http://localhost:3000/api/api/acciones-dispositivos', headers=headers, params=params)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()


# Checking microservice contenidos
microservice = "contenidos"
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiIxIiwiaWRfdXN1YXJpbyI6Im51bGwiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjAwMzMzNjI0LTU5YWQtNDVkNS04YmUyLWQzNjlkNjVlOTg4YiJ9.ss4bX8DbTBqOHAkdVtztdDjoie91oOmuM5r6flGnSRo6zPtN_zdsldDgyabP7kw7ITqx0Pg7TmSqI_r0pRLxM29R5W96By4SBug464f3HA-GR_r-3WLasguRjC3rq9pkmu_8BLyclNFYTAbSC2NlkygkvjD0NIAAJM9TLnu6P3PcjIj9bFiw-bAYk0_Otx_2hS-x31VSiA5Jq4-WKtK5ADipS3oPsZsg-neQ0Yvd9DfZBRkczI_oJg2fCNIcMtIG1gsz2zrIcJIrvsr3E9XC-cZE83FUnbxuyznEw1Sr2MxGQft-neKsKU7PWPsBfxZSAub2PX6uWpnBEoRolJePtg'),
)
# Send request and store response
response = requests.get('http://localhost:3000/api/contenidos/get', headers=headers, params=params)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()


# Checking microservice dispositivos
microservice = "dispositivos"
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiIxIiwiaWRfdXN1YXJpbyI6Im51bGwiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjAwMzMzNjI0LTU5YWQtNDVkNS04YmUyLWQzNjlkNjVlOTg4YiJ9.ss4bX8DbTBqOHAkdVtztdDjoie91oOmuM5r6flGnSRo6zPtN_zdsldDgyabP7kw7ITqx0Pg7TmSqI_r0pRLxM29R5W96By4SBug464f3HA-GR_r-3WLasguRjC3rq9pkmu_8BLyclNFYTAbSC2NlkygkvjD0NIAAJM9TLnu6P3PcjIj9bFiw-bAYk0_Otx_2hS-x31VSiA5Jq4-WKtK5ADipS3oPsZsg-neQ0Yvd9DfZBRkczI_oJg2fCNIcMtIG1gsz2zrIcJIrvsr3E9XC-cZE83FUnbxuyznEw1Sr2MxGQft-neKsKU7PWPsBfxZSAub2PX6uWpnBEoRolJePtg'),
)
# Send request and store response
response = requests.get('http://localhost:3000/api/estados-dispositivos', headers=headers, params=params)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()


# Checking microservice playlist
microservice = "playlist"
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiIxIiwiaWRfdXN1YXJpbyI6IjEiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjVhYzIxOThiLTNjNTQtNGUyMy05NDc1LTY3OGM3MDNmMzJiZiJ9.b297SAXp7duuokmnCUesEZvHpYqyEsSyPNEZ5hImMc2qDNDCNzQ65skpR9fZHyiRTwoD4weugPb00D3FRcgbGcf3HNmwIQO3h6haMaVlSxKwMGhCcHmCeRZ3bQZZv4GfYyJcdlvRNIavz9lQ00OFPx8jigUxFadQH8uQd9qUs6JfW9htYYkF6NhaiiHpyu9l35N4EHCaWO_8UPLQjFi9RhOQ6AfaGCelmRgxAcaszEZmVoeSlQ68YkSzZfRLeKOR7Bc1n_e_9YL9bEzLV_Q-xrdJZzjfYYQQm9DSXWVHChSePv_k4O9DLD1IXoJDjiqUXBMBGTVaGBnHFb3rS2cK7w'),
)
# Send request and store response
response = requests.get('http://localhost:3000/api/playlist-contenido/get', headers=headers, params=params)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()


# Checking microservice roles
microservice = "roles"
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'yJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiIxIiwiaWRfdXN1YXJpbyI6Im51bGwiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjAwMzMzNjI0LTU5YWQtNDVkNS04YmUyLWQzNjlkNjVlOTg4YiJ9.ss4bX8DbTBqOHAkdVtztdDjoie91oOmuM5r6flGnSRo6zPtN_zdsldDgyabP7kw7ITqx0Pg7TmSqI_r0pRLxM29R5W96By4SBug464f3HA-GR_r-3WLasguRjC3rq9pkmu_8BLyclNFYTAbSC2NlkygkvjD0NIAAJM9TLnu6P3PcjIj9bFiw-bAYk0_Otx_2hS-x31VSiA5Jq4-WKtK5ADipS3oPsZsg-neQ0Yvd9DfZBRkczI_oJg2fCNIcMtIG1gsz2zrIcJIrvsr3E9XC-cZE83FUnbxuyznEw1Sr2MxGQft-neKsKU7PWPsBfxZSAub2PX6uWpnBEoRolJePtg'),
)
# Send request and store response
response = requests.get('http://localhost:3000/api/roles/getroles', headers=headers, params=params)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()


# Checking microservice tags
microservice = "tags"
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiIxIiwiaWRfdXN1YXJpbyI6Im51bGwiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjAwMzMzNjI0LTU5YWQtNDVkNS04YmUyLWQzNjlkNjVlOTg4YiJ9.ss4bX8DbTBqOHAkdVtztdDjoie91oOmuM5r6flGnSRo6zPtN_zdsldDgyabP7kw7ITqx0Pg7TmSqI_r0pRLxM29R5W96By4SBug464f3HA-GR_r-3WLasguRjC3rq9pkmu_8BLyclNFYTAbSC2NlkygkvjD0NIAAJM9TLnu6P3PcjIj9bFiw-bAYk0_Otx_2hS-x31VSiA5Jq4-WKtK5ADipS3oPsZsg-neQ0Yvd9DfZBRkczI_oJg2fCNIcMtIG1gsz2zrIcJIrvsr3E9XC-cZE83FUnbxuyznEw1Sr2MxGQft-neKsKU7PWPsBfxZSAub2PX6uWpnBEoRolJePtg'),
)
# Send request and store response
response = requests.get('http://localhost:3000/api/tags', headers=headers, params=params)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()

# Checking microservice wifitrackers
microservice = "wifitrackers"
# Set port forward
setpf(kubectlpath, namespace, microservice)
# Configure request
params = (
    ('access_token', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF9yb2wiOiIxIiwiaWRfdXN1YXJpbyI6Im51bGwiLCJleHAiOjIxNDU5MTY4MDAsImp0aSI6IjAwMzMzNjI0LTU5YWQtNDVkNS04YmUyLWQzNjlkNjVlOTg4YiJ9.ss4bX8DbTBqOHAkdVtztdDjoie91oOmuM5r6flGnSRo6zPtN_zdsldDgyabP7kw7ITqx0Pg7TmSqI_r0pRLxM29R5W96By4SBug464f3HA-GR_r-3WLasguRjC3rq9pkmu_8BLyclNFYTAbSC2NlkygkvjD0NIAAJM9TLnu6P3PcjIj9bFiw-bAYk0_Otx_2hS-x31VSiA5Jq4-WKtK5ADipS3oPsZsg-neQ0Yvd9DfZBRkczI_oJg2fCNIcMtIG1gsz2zrIcJIrvsr3E9XC-cZE83FUnbxuyznEw1Sr2MxGQft-neKsKU7PWPsBfxZSAub2PX6uWpnBEoRolJePtg'),
)
# Send request and store response
response = requests.get('http://localhost:3000/api/ctx-wifitracker', headers=headers, params=params)
# Parse request
requestParser(microservice, response)
# Kill previous port-forward
killpf()


# Checking Front-End / Ingress
# FrontEnd shall be reacheble directly from internet
print("#########")
microservice = "Front End"
print("Set Front-End")
response = requests.get('http://portal.qa.demoretailsolutions.com/es/')
# Parse request
requestParser(microservice, response)


# Exit
print("")
print("Kill all portforwards")
killpf()
print("Smoke test finished")

