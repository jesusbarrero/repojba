https://enmilocalfunciona.io/creacion-test-automatizado-con-cucumber-java-selenium-y-appium/

1. Creamos un proyecto Maven

2. Configuramos el pom.xml añadiendo las siguientes dependencias

	<dependency>
   	 	<groupId>junit</groupId>
   	 	<artifactId>junit</artifactId>
    	<version>4.12</version>
    	<scope>test</scope>
	</dependency>
	<dependency>
    	<groupId>info.cukes</groupId>
    	<artifactId>cucumber-junit</artifactId>
    	<version>1.2.5</version>
    	<scope>test</scope>
	</dependency>
	<dependency>
    	<groupId>info.cukes</groupId>
    	<artifactId>cucumber-java</artifactId>
    	<version>1.2.5</version>
	</dependency>
	
--- 	PICOCONTAINER -> Inyeccion de dependencias

	<dependency>
		<groupId>info.cukes</groupId>
		<artifactId>cucumber-picocontainer</artifactId>
		<version>1.2.5</version>
		<scope>test</scope>
	</dependency>	

--- 	SELENIUM -> Automatización de test

	<dependency>
		<groupId>org.seleniumhq.selenium</groupId>
		<artifactId>selenium-java</artifactId>
		<version>3.141.59</version>
	</dependency>
	
2.1 Añadimos los siguientes plugins para poder parametrizar la clase que va a ejecutar la TestSuite
	
	<build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>3.0.0-M3</version>
        <configuration>
          <includes>
            <include>${runnerClass}.java</include>
          </includes>
        </configuration>
      </plugin>
    </plugins>
  </build>
  
  Cuando ejecutemos el proyecto mediante maven, tendremos que pasarle el nombre de la clase ejecutar valor del parámetro "runnerClass"

3. Creamos la carpeta resources/features dentro de test

4. Agregamos un nuevo fichero test.feature dentro de la carpeta features donde definiremos los escenarios de prueba siguiendo la estructura Gherking (Given-When-Then)

5. Creamos dos paquetes, uno para albergar las clases de tipo runner y otro para los stepsDefinitions

6. Creamos la clase Runner que servirá para determinar las opciones de ejecución del test. Tendrá un contenido similar al que se indica a continuación:

package es.onthespot.cucumber.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features/CucumberTest.feature",  //Ubicación del fichero feature a ejecutar
		plugin = { "pretty", "html:target/cucumber", "json:target/cucumber/#FEATUREFILE#.json" }, //formato de los resultados
		glue = {"es.onthespot.cucumber.stepDef"}  //lugar donde se encuentran las clases de definición de los pasos
		)

public class Login {

}







