nodemon - Reinicia el servidor automaticamente ante cambios en el codigo

https://www.npmjs.com/package/nodemon

express - Popular libreria para crear servidores personalizados

https://www.npmjs.com/package/express

morgan - middleware

https://www.npmjs.com/package/morgan

dotenv - nos permite acceder al valor de las variables de entorno de un fichero .env

https://www.npmjs.com/package/dotenv

mongoose - Conexion a mongoDB

https://www.npmjs.com/package/mongoose

body parser - Por defecto, la libreria express oculta el contenido del body de las peticiones. Permite mostrar el contenido del body en las peticiones a nuestra API

https://www.npmjs.com/package/body-parser

express-validator - handle errors an give proper error messages

https://www.npmjs.com/package/express-validator
https://express-validator.github.io/docs/

superagent - cliente HTTP

https://visionmedia.github.io/superagent/