const s3Bucketname = 'spotdyna-qa-generic-content/sthilodes/HMSIP2/sw'

const s3fs = require('s3fs');
const s3bucket = new s3fs(s3Bucketname)

exports.listBucket = async (path,prefix) => {
    const ruta = await s3bucket.getPath()
    console.log(ruta)
    return s3bucket.listContents(path,prefix)
}

exports.writeFileToBucket = async (filename,data) => {
    //return s3bucket.(`${localFile}`,`${remotePath}/${localFile}`)
    return s3bucket.writeFile(filename,data)
    .then( () => {
        console.log(`Se subio el fichero ${filename} al bucket ${s3bucket.getPath()}`)
    })
    .catch(err => {
        console.error(err)
    })
}
