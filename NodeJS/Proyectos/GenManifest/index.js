const crypto = require('crypto')
const fs = require('fs')
const AdmZip = require('adm-zip')

const zipFile = new AdmZip();
const list = fs.readdirSync('updateFiles')

const manifest = [] 
for (i=0;i<list.length;i++){
    file = fs.readFileSync(`updateFiles/${list[i]}`)
    hash = crypto.createHash('md5').update(file).digest('hex')
    order = i+1+""
    manifest.push({
        filename : list[i],
        md5 : hash,
        order: order
    })
}
fs.writeFileSync('manifest.json',JSON.stringify(manifest))
console.log(manifest)


zipFile.addLocalFolder('updateFiles')
zipFile.addLocalFile('manifest.json')
zipFile.writeZip('REGENPROG_DSANDROID.zip')
console.log("Se generó el fichero REGENPROG_DSANDROID.zip")


// const crypto = require('crypto');
// file = '/c/Users/jesus.barrero/Downloads/5864_5165.mp4';
// console.log(crypto.createHash('md5').update(file).digest('hex'))