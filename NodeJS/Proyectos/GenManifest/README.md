Aplicación que genera el fichero update.zip para la actualización automatica de terminales MKD Android por Manifest

https://onthespot.atlassian.net/wiki/spaces/QASite/pages/1399357441/Spotsign+Hybroad+-+Nuevo+modelo+de+actualizaci+n+versiones+superiores+a+partir+de+mayo+2020

Se deben incluir los ficheros objetivo en la carpeta updateFiles. Se generará un fichero zip que incluye todos los archivos que haya en la carpeta updateFiles + el fichero manifest.json

Se ejecuta el script con el comando npm run test