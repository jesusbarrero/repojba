const {testRunId,getTest,getSpotdynaScenarioTags,wait, runTest,addTagToTest} = require('./apiRequest/cucumberApiRequest')
const {getScenarioWithJiraIssueTodo} = require('./apiRequest/jiraApiRequest')

const p = process.env

const testForRetest = async (testList,scenarioWithJiraIssueTodo) => {
  
    listTestToRetest = []
    scenarioWithJiraIssueTodo.forEach((scenario) => {
        testToRetest = testList.find((test) =>{
            return test.scenario.includes(scenario)
        })
        listTestToRetest.push(testToRetest)
        return
    })
    return listTestToRetest
}

const markTestRetest = async (testList) => {
    
    for(let i=0;i<testList.length;i++){
        await runTest(testList[i].testId,'retest','autoRetest','Test marcado para retest por posible bug pendiente de revision')
        console.log(testList[i].testSummary)
        await addTagToTest(testList[i].testId,'AUTO','RETEST')
    }
    return
}

const prueba = async () => {

    // testList = [
    //     {
    //     testId: '20978824',
    //     testSummary: 'coco1',
    //     scenario: 'cucu1'
    //     },
    //     {
    //         testId: '20978825',
    //         testSummary: 'coco2',
    //         scenario: 'cucu2'
    //     },
    //     {
    //         testId: '20978826',
    //         testSummary: 'coco3',
    //         scenario: 'cuc3'
    //     }
    // ]
    
    const testList = await getTest(p.testRunId)
    console.log('Recopilando incidencias de JIRA asociadas a los escenarios del test...')
    const listScenarioTags = await getSpotdynaScenarioTags(testList)
    const scenarioWithJiraIssueTodo = await getScenarioWithJiraIssueTodo(listScenarioTags)
    const listTestToRetest = await testForRetest(testList,scenarioWithJiraIssueTodo)
    await markTestRetest(listTestToRetest)
}

prueba();