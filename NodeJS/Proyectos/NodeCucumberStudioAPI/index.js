const {testRunId,getTest,getTestIdFromKey, createBugScenario, addTagToScenario,runTest,addTagToTest, addScenarioToTestRun,wait} = require('./apiRequest/cucumberApiRequest')
const {getJiraIssue} = require('./apiRequest/jiraApiRequest')

const p = process.env

const prueba = async (key) => {

    const jiraIssue = await getJiraIssue(key)
    const scenarioId = await createBugScenario(jiraIssue.summary)
    await addTagToScenario('JIRA',key,scenarioId)
    await addScenarioToTestRun(scenarioId)
    console.log('Esperando la sincronizacion del testRun...')
    await wait(10000)
    const testId = await getTestIdFromKey(key)
    await runTest(testId,p.status,p.name,'')
    await addTagToTest(testId,p.tag,'')
}

prueba(p.jiraIssue)

// (lista) => {
//     lista.forEach(element => {
//         console.log(element)
//     });
// }


