#!/bin/bash

echo "Script para asociar test de Cucumber Studio a tareas de JIRA"

echo "Test Run ID"

read testRunId

echo "Introduce el codigo de la incidencia de JIRA (SPOTDYNA-XXXX)"

read jiraIssue

echo "Nombre: "

read name

echo "Tag: "

read tag

echo "Ejecucion del test: (passed/failed) "

read status

echo "testRunId=$testRunId jiraIssue=$jiraIssue name=$name tag=$tag status=$status npm run test"

testRunId=$testRunId jiraIssue=$jiraIssue name=$name tag=$tag status=$status npm run test
