const superagent = require('superagent');
const fs = require('fs');
const projectId = '80548'
const testRunId = process.env.testRunId
const bugFolderId = '1002173' 

const config = JSON.parse(fs.readFileSync('config.json','utf-8'));
const cucumberHeaders = {
    uid: 'testing.telefonica17@gmail.com',
    client: 'W-x3mM5uy-2TrAhoJjOjlA',
    'access-token': config.cucumberToken,
    Accept: 'application/vnd.api+json; version=1'
}

const postRequest = (endpoint,cucumberHeaders,payload) => superagent.post(endpoint).set(cucumberHeaders).send(payload)
const patchRequest = (endpoint,cucumberHeaders,payload) => superagent.patch(endpoint).set(cucumberHeaders).send(payload)
const getRequest = (endpoint,cucumberHeaders) => superagent.get(endpoint).set(cucumberHeaders)

exports.wait = (ms) => {
    return new Promise(resolve => setTimeout(resolve,ms))
}

exports.getTest = async (testRunId) => {

    endpoint = `${config.baseUrl}api/projects/${projectId}/test_runs/${testRunId}/test_snapshots/`
    listTest = []

    return getRequest(endpoint,cucumberHeaders)
    .query({include: 'scenario'})
    .then(res => {
        res.body.data.forEach(data =>{
            listTest.push({
                testId: data.id,
                testSummary: data.attributes.name,
                scenario: data.relationships.scenario.data.id
            })
        })
    })
    .then(() => {
        console.log('Generada lista de test')
        return listTest
    })
    .catch(err =>{
        console.log('error: ' + err.message)
    })
}


exports.createScenario = async (summary,folderId) => {

    endpoint = `${config.baseUrl}api/projects/${projectId}/scenarios`
    payload = {
        data : {
            attributes : {
                name : summary,
                'folder-id' : folderId,
            }
        }
    }


    return postRequest(endpoint,cucumberHeaders,payload)
    .then(res => {
        console.log("Escenario creado")
        return res.body.data.id
    })
    .catch(err =>{
        console.log('error: ' + err.message)
    })
}

exports.createBugScenario = async (summary) => {
    return this.createScenario(summary,bugFolderId)
}

exports.addTagToScenario = async (tagKey, tagValue,scenarioId) => {

    endpoint = `${config.baseUrl}api/projects/${projectId}/scenarios/${scenarioId}/tags`
    payload = {
        data: {
            attributes : {
                key: tagKey,
                value: tagValue
            }
        }
    }

    return postRequest(endpoint,cucumberHeaders,payload)
    .then(res => {
        console.log("Se añadio el tag al escenario")
    })
    .catch(err =>{
        console.log('error: ' + err.message)
    })
}

exports.addScenarioToTestRun = async (scenarioId) =>{

    endpoint = `${config.baseUrl}api/projects/${projectId}/test_runs/${testRunId}`
    payload = {
        data: {
            type: 'test_runs',
            id: testRunId,
            attributes : {
                'scenario_id': scenarioId
            }
        }
    }

    return patchRequest(endpoint,cucumberHeaders,payload)
    .then(res =>{
        console.log('Se añadió escenario al testRun')
    })
    .catch(err =>{
        console.log('error: ' + err.message + scenarioId)
    })
}

exports.getTestIdFromKey = async (key) => {
    
    return this.getTest(testRunId)
    .then((list) => list.find((a) => a.testSummary.includes(key)))
    .then((testId) => {
        if (testId){
            return testId.testId
        }else{
            console.log("No se encontró")
            return false
        }
    })
    .catch(err =>{
        console.log('error: ' + err.message)
    })
    
    
    // return listTest.find((a)=>{
    //     a.testSummary.includes(key)
    // })
}

exports.runTest = async (testId,status,author,description) => {

    endpoint = `${config.baseUrl}api/projects/${projectId}/test_runs/${testRunId}/test_snapshots/${testId}/test_results`
    payload = {
        data: {
            type: 'test-results',
            attributes: {
                status: status,
                'status-author': author,
                description: description
            }
        }
    }

    return postRequest(endpoint,cucumberHeaders,payload)
    .then(() => {
        console.log(`Se marcó el test a ${status}`)
        return
    })
    .catch(err =>{
        console.log('error: ' + err.message)
    })

}

exports.addTagToTest = async (testId,tagKey,tagValue) => {

    endpoint = `${config.baseUrl}api/projects/${projectId}/test_runs/${testRunId}/test_snapshots/${testId}/tags`
    payload = {
        data: {
            attributes: {
                key: tagKey,
                value: tagValue
            }
        }
    }

    return postRequest(endpoint,cucumberHeaders,payload)
    .then(() => {
        console.log(`Se añadió el tag ${tagKey} ${tagValue}`)
        return
    })
    .catch(err =>{
        console.log('error: ' + err.message)
    })
}

exports.getTagFromScenario = async (scenarioId) => {

    endpoint =  `${config.baseUrl}api/projects/${projectId}/scenarios/${scenarioId}/tags`
    listTags = []

    return getRequest(endpoint,cucumberHeaders)
    .then((res) =>{
        res.body.data.forEach((d) => {
            listTags.push({
                idTag: d.id,
                tagKey: d.attributes.key,
                tagValue: d.attributes.value
            })
        })
    })
    .then(() => {
        //console.log("Se obtuvo la lista de tags del escenario")
        return listTags
    })
    .catch(err =>{
        console.log('error: ' + err.message)
    })
}

exports.getSpotdynaScenarioTags = async (scenarioList) => {

    scenarioWithSpotdynaIssue = []
    for (let i=0; i<scenarioList.length; i++){
        const lista = await this.getTagFromScenario(scenarioList[i].scenario)
        spotdynaIssues = []
        for(j=0;j<lista.length;j++){
        if(lista[j].tagKey.includes('JIRA')){
        spotdynaIssues.push(lista[j].tagValue)
        // scenarioTags.push({
        //     scenarioId: scenarioList[i].scenario,
        //     scenarioTags: lista
        //         })
        //     encontrado = true
        //     }
        }        
    }
    if (spotdynaIssues.length > 0){
    scenarioWithSpotdynaIssue.push({
        scenarioId: scenarioList[i].scenario,
        spotdynaIssues: spotdynaIssues
    })
    }
}
    return scenarioWithSpotdynaIssue
}








