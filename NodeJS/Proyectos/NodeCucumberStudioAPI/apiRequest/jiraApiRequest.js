const superagent = require('superagent');
const fs = require('fs');

const config = JSON.parse(fs.readFileSync('config.json','utf-8'));
const jiraHeaders = {
    Authorization: `Basic ${config.jiraToken}`,
    Accept: 'application/json',
    'Content-Type': 'application/json'
}
const jiraEndpoint = 'onthespot.atlassian.net'

exports.getJiraIssue = (jiraIssue) => {

    const getAnIssue = `https://${jiraEndpoint}/rest/api/3/issue/${jiraIssue}`
    
    return superagent
    .get(getAnIssue)
    .set(jiraHeaders)
    .then(res => {
        return {
            summary: `[${jiraIssue}] - ${res.body.fields.summary}`,
            issueType: res.body.fields.issuetype.name,
            status: res.body.fields.status.name
        }
    })
    .catch(err =>{
        console.log('error: ' + err.message)
    })
}

exports.getScenarioWithJiraIssueTodo = async (listScenarioTags) => {

    listScenarioWithJiraIssueTodo = []
    for (let i=0; i<listScenarioTags.length;i++){
        j=0
        encontrado = false
        while(j<listScenarioTags[i].spotdynaIssues.length && !encontrado){
            //Check if jiraIssue is todo or reopen
            //listScenarioTags[i].spotdynaIssues[j]
            try{
            jiraIssue = await this.getJiraIssue(listScenarioTags[i].spotdynaIssues[j])
            s = jiraIssue.status
            if (s.includes('Reabierta') || s.includes('Tareas por hacer')){
                listScenarioWithJiraIssueTodo.push(listScenarioTags[i].scenarioId)
                encontrado = true
                console.log(`El escenario ${listScenarioTags[i].scenarioId} esta asociado al JIRA ${jiraIssue.summary} que se encuentra en estado ${s}`)
            }
            }catch{
                console.log(`No se encontró el JIRA ${listScenarioTags[i].spotdynaIssues[j]}`)
            }finally{
                j++        
            }
        }
    }
    return listScenarioWithJiraIssueTodo
}