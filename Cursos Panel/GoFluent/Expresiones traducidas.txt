using our own cash and small loans
utilizando nuestro propio efectivo y pequeños préstamos

But everyone turned us down.
Pero todos nos rechazaron. 

Larry Cheng at Volition Capital was one of the people we pitched our company to
Larry Cheng en Volition Capital fue una de las personas a las que les propusimos nuestra compañía

He followed up with us about six months later
Él siguió con nosotros unos seis meses después

From that point on, our mission was straightforward
A partir de ese momento, nuestra misión fue sencilla

to build a best-in-class, customer-obsessed pet retailer
para construir el mejor minorista de mascotas obsesionado con el cliente

Consultants had told us that it would take a year and a half to build a warehouse from scratch
Los consultores nos dijeron que llevaría un año y medio construir un almacén desde cero

When we were finally staffed, we were tackling issue after issue 24/7 until we worked out all the kinks.
Cuando finalmente contamos con personal, abordamos un problema tras otro las 24 horas del día, los 7 días de la semana, hasta que resolvimos todos los problemas.

Mr. Davis is known to be the most gracious manager in the company
Se sabe que el Sr. Davis es el gerente más amable de la empresa.

She buys the best cloth for the dresses she sews for her clients
Ella compra la mejor tela para los vestidos que cose para sus clientes.

May takes pride in having the neatest hemlines; she always checks to see that there are no stray threads poking out of her finished products.
May se enorgullece de tener los dobladillos más limpios; ella siempre verifica que no haya hilos sueltos saliendo de sus productos terminados.

I remember when we had to wait for months to get our photographs printed. We didn't have digital cameras back then.
Recuerdo cuando tuvimos que esperar meses para imprimir nuestras fotografías. No teníamos cámaras digitales en ese entonces.

What this means is you will save three to five hours a week on cleaning your coffeemaker.
Lo que esto significa es que ahorrará de tres a cinco horas a la semana en la limpieza de su cafetera.ç

I feel like we can all end up prattling on
Siento que todos podemos terminar parloteando

You cannot go on a spending spree whereas you feel like it just whenever because you have a credit card.
No puede ir de juerga mientras siente que tiene ganas porque tiene una tarjeta de crédito.

I've seen many of my friends and colleagues indulge themselves in too much shopping while struggling to earn the money to pay for it.
He visto a muchos de mis amigos y colegas darse el gusto de comprar demasiado mientras luchan por ganar el dinero para pagarlo.

Some of them had to terminate their credit cards after only a few months when they realized they were in over their heads.
Algunos de ellos tuvieron que cancelar sus tarjetas de crédito después de solo unos pocos meses cuando se dieron cuenta de que estaban sobre sus cabezas.

Those segments of the map are overwhelmingly represented in just about every experience
Esos segmentos del mapa están abrumadoramente representados en casi todas las experiencias.

Unforeseen circumstances
https://translate.google.es/?hl=es&tab=wT#view=home&op=translate&sl=en&tl=es&text=Due%20to%20unforeseen%20circumstances%2C%20we%20will%20have%20to%20wait%20a%20few%20hours%20before%20we%20can%20take%20off.

Thus
https://translate.google.es/?hl=es&tab=wT#view=home&op=translate&sl=en&tl=es&text=Thus%2C%20we%20will%20wait%20for%20air%20traffic%20control%20to%20issue%20the%20clearance%20for%20this%20flight.

Pose danger
https://translate.google.es/?hl=es&tab=wT#view=home&op=translate&sl=en&tl=es&text=the%20birds%20in%20the%20sky%20pose%20danger%20to%20the%20airplane's%20turbine%20engines

endure
https://translate.google.es/?hl=es&tab=wT#view=home&op=translate&sl=en&tl=es&text=taught%20her%20lessons%20that%20can%20help%20her%20endure%20self-isolation

larking
https://translate.google.es/?hl=es&tab=rT#view=home&op=translate&sl=en&tl=es&text=apparently%20his%20grandchildren%20--%20larking%20about%20in%20the%20corner.

Second violinist Sohei Birmann, 35, was more bullish about the teleworking trial initially.
https://translate.google.es/?hl=es&tab=rT#view=home&op=translate&sl=en&tl=es&text=Second%20violinist%20Sohei%20Birmann%2C%2035%2C%20was%20more%20bullish%20about%20the%20teleworking%20trial%20initially.

As the coronavirus spreads in Japan, there are growing fears that Prime Minister Shinzo Abe will soon declare a state of emergency, paving the way for greater restrictions on movement.
https://translate.google.es/?hl=es&tab=rT#view=home&op=translate&sl=en&tl=es&text=As%20the%20coronavirus%20spreads%20in%20Japan%2C%20there%20are%20growing%20fears%20that%20Prime%20Minister%20Shinzo%20Abe%20will%20soon%20declare%20a%20state%20of%20emergency%2C%20paving%20the%20way%20for%20greater%20restrictions%20on%20movement.
