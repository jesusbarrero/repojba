CERT B1

Te encuentras cómodo/a en conversaciones largas sobre temas con los que estás familiarizado/a.
Puedes expresar tu opinión y ofrecer explicaciones detalladas. Ocasionalmente puedes encontrar dificultades en cuestiones de vocabulario y gramática.
Puedes mantener conversaciones aunque no las hayas preparado con anterioridad y resolver problemáticas de tu día a día.