package selenium;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class TestEvents {

    private static WebDriver driver;
    private static EventFiringWebDriver eventDriver;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver","D:\\Chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        // Necesario instanciar un objeto de la clase EventFiringWebDriver
        eventDriver = new EventFiringWebDriver(driver);
        // Registramos la clase donde hemos definido las acciones a realizar en cada evento
        TraceListener traceListener = new TraceListener();
        eventDriver.register(traceListener);
    }

    @Test
    public void goToGoogle(){
        eventDriver.get("https://www.google.es");
        eventDriver.findElement(By.id("no-existe"));
    }

    @AfterClass
    public static void tearDown(){
        eventDriver.quit();
    }
}
