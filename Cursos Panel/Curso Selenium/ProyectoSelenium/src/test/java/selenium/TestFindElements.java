package selenium;

import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

// Importamos todos los tipos de aserciones de la libreria de junit
import java.util.List;

import static org.junit.Assert.*;

public class TestFindElements {

    private static WebDriver driver;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver","D:\\Chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void findByClass(){
        driver.get("http://todomvc.com/examples/vue/");
        WebElement miInput = driver.findElement(By.className("new-todo"));
        String placeholder = miInput.getAttribute("placeholder");
        assertEquals("What needs to be done?",placeholder);
    }

    @Test
    public void findById(){
        driver.get("http://localhost:8081/index.html");
        WebElement titulo = driver.findElement(By.id("titulo-principal"));
        assertEquals("Titulo",titulo.getText());
    }

    @Test
    public void findByName(){
        driver.get("http://localhost:8081/index.html");
        WebElement password = driver.findElement(By.name("password"));
        assertEquals("text",password.getAttribute("type"));
    }

    @Test
    public void getValue(){
        driver.get("http://localhost:8081/index.html");
    //  WebElement password = driver.findElement(By.className("form-control"));
        // Si queremos buscar en mas de un elemento, utilizamos findElements
        List<WebElement> listaFormControl = driver.findElements(By.className("form-control"));
        assertEquals("1234",listaFormControl.get(1).getAttribute("value"));
        assertEquals("Jesus",listaFormControl.get(0).getAttribute("value"));
    }

    @Test
    public void findByTag(){
        driver.get("http://localhost:8081/index.html");

        // Buscamos la etiqueta formulario
        WebElement formulario = driver.findElement(By.tagName("form"));

        // Buscamos el elemento cuyo nombre es usuario
        WebElement usuario = formulario.findElement(By.name("usuario"));

        // Verificamos el valor de este campo usuario
        assertEquals("Jesus",usuario.getAttribute("value"));
    }

    @Test
    public void findByLink(){
        driver.get("http://localhost:8081/index.html");

        // Para trabajar con enlaces usamos linkText y buscamos por el texto dentro de la etiqueta a
        // Si hay una parte del enlace que pueda cambiar usaremos partialLinkText
        WebElement enlaceNetflix = driver.findElement(By.linkText("Netflix"));
        assertEquals("http://www.netflix.com",enlaceNetflix.getAttribute("href"));

        WebElement enlaceGmail = driver.findElement(By.partialLinkText("Emails"));
        assertEquals("http://www.gmail.com",enlaceNetflix.getAttribute("href"));

        /*
        Este codigo no se ejecuta correctamente

        enlaceNetflix.click();
        String titulo = driver.getTitle();
        assert titulo.contains("Netflix") : " >>> El titulo mostrado es " + titulo ;
         */
    }

    @Test
    public void findByXpath(){
        driver.get("http://localhost:8081/index.html");
        // WebElement botonSubmit = driver.findElement(By.xpath("/html/body/form/button"));
        // WebElement botonSubmit = driver.findElement(By.xpath("//button"));
        // assertEquals("Enviar",botonSubmit.getText());

        // Busca de entre todos los inputs, el que tiene un atributo type=password
        //WebElement inputPassword = driver.findElement(By.xpath("//input[@type='password']"));
        //assertEquals("1234",inputPassword.getAttribute("value"));

        // Busca de entre todos los form, en el segundo div, un elemento con etiqueta input
        WebElement inputPassword = driver.findElement(By.xpath("//form/div[2]/input"));
        assertEquals("1234",inputPassword.getAttribute("value"));
    }

    @Test
    public void ejercicio5(){
        driver.get("http://www.w3schools.com/html/html_tables.asp");

        // Encontrar la tabla de la página que tiene la cabecera HTML Table Example
        WebElement tabla = driver.findElement(By.id("customers"));

        // Comprueba que la tabla existe
        assertNotNull(tabla);

        // Comprueba que tiene el número de filas correcto
        List<WebElement> listElements = tabla.findElements(By.tagName("tr"));
        assertEquals(7,listElements.size());

        // Comprueba que la última fila tiene el número de celdas correcto

        WebElement ultimaFila = listElements.get(listElements.size()-1);
        Integer numeroDeColumnas = ultimaFila.findElements(By.tagName("td")).size();
        assert numeroDeColumnas.equals(3);

        // Comprueba que después de la quinta fila, hay dos filas más

        //WebElement quintaFila = listElements.get(5);
        //Integer restoDeFilas = quintaFila.findElements(By.xpath("./tr")).size();

        //Obtengo todas las fiasl que hay a partir de la posticion 5
        List<WebElement> ultimas2Filas = tabla.findElements(By.xpath("./tbody/tr[position() > 5]"));
        Integer numero = ultimas2Filas.size();
        assert numero.equals(2);

        // Comprueba que todas las celdas tienen contenido

        List<WebElement> celdas = tabla.findElements(By.xpath(".//td"));

        for (WebElement e : celdas){
            assertNotNull(e.getText());
            System.out.println(e.getText());
        }
        
    }

    @Test
    public void findByCSS(){
        driver.get("http://localhost:8081/index.html");

        WebElement enlace = driver.findElement(By.cssSelector("a[href$='.es'"));
        assertEquals("http://www.netflix.es/",enlace.getAttribute("href"));

        WebElement div1 = driver.findElement(By.cssSelector("form div:first-child"));
        WebElement div2 = driver.findElement(By.cssSelector("form div:nth-child(2)"));
        WebElement btn = driver.findElement(By.cssSelector("form div:last-child"));

        assertEquals("Enviar", btn.getText());
        assertEquals("div1", div1.getAttribute("id"));
        assertEquals("div2", div2.getAttribute("id"));

        WebElement inputUsuario = driver.findElement(By.cssSelector("input:focus"));
        assertEquals("Jesus", inputUsuario.getAttribute("value"));
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }
}
