package selenium;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class TestInteracciones {

    private static WebDriver driver;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver","D:\\Chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void testSendKeys() throws InterruptedException {

        driver.get("http://todomvc.com/examples/vanillajs");

        WebElement buscador = driver.findElement(By.className("new-todo"));
        buscador.sendKeys("Tarea 1");
        // buscador.submit(); // El metodo submit solo es válido si el elemento está dentro de una etiqueta form
        buscador.sendKeys(Keys.ENTER);
        buscador.sendKeys("Tarea 2");
        // Si limpiamos el campo antes de pulsar enter, no se crea una nueva fila
        buscador.clear();

        /* WebDriverWait w = new WebDriverWait(driver,2);
        driver.wait(until(w)); */
        buscador.sendKeys(Keys.ENTER);

        // Busca un elemento ul de la clase todo-list y de ahi obtiene todos los hijos de la etiqueta li con clase todo
        // List<WebElement> listaTareas = driver.findElements(By.cssSelector("ul.todo-list > li.todo"));

        // Busca el elemento ul de la clase todo y seleccionamos todos los hijos li
        List<WebElement> listaTareas = driver.findElements(By.xpath("//ul[@class='todo-list']/li"));

        assertEquals(1,listaTareas.size());
    }

    @Test
    public void buscaEnGoogle(){
        driver.get("http://www.google.es");
        WebElement q = driver.findElement(By.name("q"));
        q.sendKeys("Selenium");
        WebElement buttonGoogle = driver.findElement(By.name("btnK"));
        buttonGoogle.click(); //Click sobre el boton de busqueda de google
        // q.submit(); // Enviar el contenido que se haya escrito en el buscador

        String tituloWeb = driver.getTitle();
        assert tituloWeb.contains("Selenium") : ">> El titulo de la web es " + tituloWeb;
    }

    @Test
    public void cssProperties(){

        driver.get("http://todomvc.com/examples/vanillajs");

        WebElement buscador = driver.findElement(By.className("new-todo"));
        buscador.sendKeys("Tarea completada");
        buscador.sendKeys(Keys.ENTER);
        WebElement checkbox = driver.findElement(By.xpath("//input[@class='toggle']"));
        checkbox.click();

        buscador.sendKeys("Tarea no completada");
        buscador.sendKeys(Keys.ENTER);

        String colorCompletado = "rgba(217, 217, 217,1)";

        // En el inspector podemos ver los distintos selectores CSS asociados a un elemento
        WebElement tareaCompletada = driver.findElement(By.cssSelector(".todo-list li.completed label"));

        assertEquals(colorCompletado,tareaCompletada.getCssValue("color"));
    }

    @Test
    public void getSelect(){
        driver.get("http://localhost:8081/index.html");
        WebElement desplegableCoches = driver.findElement(By.id("coches"));

        // Instanciamos un objeto de la clase Select a traves del WebElement con etiqueta select
        Select selectCoches = new Select(desplegableCoches);
        selectCoches.selectByVisibleText("Tesla");
        assertEquals("Tesla",selectCoches.getFirstSelectedOption().getText());

        selectCoches.selectByIndex(1);
        assertEquals("Renault", selectCoches.getFirstSelectedOption().getText());

        selectCoches.selectByValue("nio");
        assertEquals("nio", selectCoches.getFirstSelectedOption().getAttribute("value"));
    }

    @Test
    public void getSelectMultiple(){
        driver.get("http://localhost:8081/index.html");
        WebElement desplegableSeries = driver.findElement(By.id("series"));

        // Instanciamos un objeto de la clase Select a traves del WebElement con etiqueta select
        Select selectSeries = new Select(desplegableSeries);
        selectSeries.selectByValue("theLeftovers");
        selectSeries.selectByIndex(0);

        List<WebElement> seriesElegidas = selectSeries.getAllSelectedOptions();
        assertTrue(selectSeries.isMultiple());
        assertEquals(2,seriesElegidas.size());

        selectSeries.deselectByValue("theLeftovers");
        seriesElegidas = selectSeries.getAllSelectedOptions();
        assertEquals(1,seriesElegidas.size());

        selectSeries.deselectAll();
        seriesElegidas = selectSeries.getAllSelectedOptions();
        assertEquals(0,seriesElegidas.size());
    }

    @Test
    public void radioButtons(){
        driver.get("http://localhost:8081/index.html");
        List<WebElement> radioButtons = driver.findElements(By.cssSelector("input[type='radio']"));
        radioButtons.get(1).click();

        assertTrue(radioButtons.get(1).isSelected());
        assertFalse(radioButtons.get(0).isSelected());

        radioButtons.get(0).click();

        assertTrue(radioButtons.get(0).isSelected());
        assertFalse(radioButtons.get(1).isSelected());
    }

    @Test
    public void checkbox() {
        driver.get("http://localhost:8081/index.html");
        WebElement checkT = driver.findElement(By.id("check1"));
        WebElement checkF = driver.findElement(By.id("check2"));
        WebElement checkS = driver.findElement(By.id("check3"));

        assertTrue(checkT.isSelected());
        if (checkT.isSelected()){
            checkT.click();
        }
        assertFalse(checkT.isSelected());

        checkF.click();
        checkS.click();


        // List<WebElement> checkboxesSeleccionados = driver.findElements(By.xpath("//input[@type='checkbox and @checked]"));
        List<WebElement> checkboxesSeleccionados = driver.findElements(By.cssSelector("input[type='checkbox']:checked"));
        // Se verifica la pseudoclase checked
        // Podemos verlo en la pestaña Accessibility > Computed properties
        // En javascript -> document.querySelectorAll('input:checked')

        assertEquals(2, checkboxesSeleccionados.size());
    }

    @Test
    public void doubleClick(){
        driver.get("http://cookbook.seleniumacademy.com/DoubleClickDemo.html");
        WebElement caja = driver.findElement(By.id("message"));

        assertEquals("rgba(0, 0, 255, 1)", caja.getCssValue("background-color"));

        Actions builder = new Actions(driver);
        builder.doubleClick(caja).perform();

        assertEquals("rgba(255, 255, 0, 1)", caja.getCssValue("background-color"));
    }

    @Test
    public void dragAndDrop(){
        driver.get("http://cookbook.seleniumacademy.com/DragDropDemo.html");
        WebElement elementToDrag = driver.findElement(By.id("draggable"));
        WebElement dropElement = driver.findElement(By.id("droppable"));

        Actions builder = new Actions(driver);
        //builder.dragAndDrop(elementToDrag,dropElement).perform();

        //Drag and drop en varias acciones
        builder.clickAndHold(elementToDrag).moveToElement(dropElement).release().perform();

        assertEquals("Dropped!",dropElement.getText());
    }

    @Test
    public void moveAndClick() {
        driver.get("http://www.google.es");
        WebElement q = driver.findElement(By.name("q"));
        q.sendKeys("Selenium");

        Actions builder = new Actions(driver);
        builder.moveByOffset(62, 118).click();

        WebElement buttonGoogle = driver.findElement(By.name("btnK"));
        buttonGoogle.click(); //Click sobre el boton de busqueda de google

    }

    @Test
    public void alert(){
        driver.get("http://localhost:8081/index.html");

        /*
        //Hemos definido el script mostrarMensaje() en el el fichero index.html

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("mostrarMensaje()");
        */


        WebElement buttonAlert = driver.findElement(By.id("btn-alert"));
        buttonAlert.click();

        Alert alert = driver.switchTo().alert();
        assertEquals("Un mensaje cualquiera...", alert.getText());
        alert.accept();
    }

    @Test
    public void screenshot(){

        driver.get("http://localhost:8081/index.html");
        WebElement desplegableSeries = driver.findElement(By.id("series"));

        // Instanciamos un objeto de la clase Select a traves del WebElement con etiqueta select
        Select selectSeries = new Select(desplegableSeries);
        selectSeries.selectByValue("theLeftovers");
        selectSeries.selectByIndex(0);

        List<WebElement> seriesElegidas = selectSeries.getAllSelectedOptions();
        assertTrue(selectSeries.isMultiple());
        assertEquals(2,seriesElegidas.size());

        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile,new File("src/test/resources/screenshots/antes.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        selectSeries.deselectByValue("theLeftovers");
        seriesElegidas = selectSeries.getAllSelectedOptions();
        assertEquals(1,seriesElegidas.size());



    }

    @Test
    public void ejercicioCookies(){
        driver.get("http://es.wikipedia.org/wiki/wikipedia:Portada");

        // Comprobar que hay una cookie con el nombre GeoIP

        assertNotNull(driver.manage().getCookieNamed("GeoIP"));

        // Obtener todas las cookies

        Set<Cookie> allCookies = driver.manage().getCookies();
        assertTrue(allCookies.size() > 0);

        // Borrar las cookies y comprobar que no hay

        driver.manage().deleteAllCookies();
        allCookies = driver.manage().getCookies();

        System.out.println(" Se encontraron " + allCookies.size() + " cookies");
    }

    @AfterClass
    public static void tearDown(){
       driver.quit();
    }
}
