package selenium;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@RunWith(Parameterized.class)
public class TestDDT {

    private static WebDriver driver;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver","D:\\Chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void goToGoogle(){
        driver.get("https://www.google.es");
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }
}
