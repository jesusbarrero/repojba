package selenium;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.junit.Assert.*;

public class TestWaits {

    private static WebDriver driver;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver","D:\\Chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void implicitWait(){
        driver.get("http://localhost:8081/boton-esperas/index.html");
        driver.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);
        WebElement btn =  driver.findElement(By.id("btn"));
        assertEquals("Pulsa aquí",btn.getText());
    }

    @Test
    public void explicitWait(){
        driver.get("http://localhost:8081/boton-esperas/index.html");
        //Como mucho espera 4 segundos
        WebDriverWait wait = new WebDriverWait(driver, 4, 300);

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("btn")));

        WebElement btn =  driver.findElement(By.id("btn"));
        assertEquals("Pulsa aquí",btn.getText());
    }

    @Test
    public void fluentWait(){
        driver.get("http://localhost:8081/boton-esperas/index.html");

        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(4)) // Espera 4 segundos
                .pollingEvery(Duration.ofMillis(300)) // Comprueba cada 300 milisegundos
                .ignoring(Exception.class); // Ignora las excepciones

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("btn")));
        // Se pueden definir funciones personalizadas
        // wait.until(Function.identity());

        WebElement btn =  driver.findElement(By.id("btn"));
        assertEquals("Pulsa aquí",btn.getText());
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }
}
