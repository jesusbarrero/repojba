#!/bin/bash

#El primer argumento es el nombre del fichero de captura

echo "screencap.sh <nombreFicheroSinExtension>"

outFilePath="/d/Panel/OntheSpot/MKDAndroid/Pruebas/Capturas/"
outFilePathWin='D:\\Panel\\Onthespot\\MKDAndroid\\Pruebas\\Capturas\\'

adb shell screencap -p sdcard/$1.png
adb pull sdcard/$1.png $outFilePath
adb shell rm sdcard/$1.png
mspaint $outFilePathWin$1.png &
