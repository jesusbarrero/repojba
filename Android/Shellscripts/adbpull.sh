#!/bin/bash

# Descarga de los ficheros de configuracion de MKDAndroid
# example:
# . adbpull <tipoFichero> <idDispositivo>

otsFilePath='data/data/com.onthespot.androidplayer/files/OTS/'
localFilePath='mnt/mmcblk1/mmcblk1p1/Android/data/com.onthespot.androidplayer/files/OTS'
localPath='/d/Panel/Onthespot/MKDAndroid/Ficheros/'
idDispositivo=$2
localPathIni=$localPath'Ini/'$idDispositivo.json

while :
do 
case $1 in
    "ini") echo "Descargando el fichero ini..."
    adb pull $otsFilePath'hmsipplayer.json' $localPathIni
    code $localPathIni
    break
    ;;
esac
done

code $localPathIni &