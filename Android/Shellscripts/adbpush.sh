#!/bin/bash

# Subida de los ficheros de configuracion de MKDAndroid
# example:
# . adbpush <tipoFichero> <idDispositivo>

otsFilePath='data/data/com.onthespot.androidplayer/files/OTS/'
localFilePath='mnt/mmcblk1/mmcblk1p1/Android/data/com.onthespot.androidplayer/files/OTS'
localPath='/d/Panel/Onthespot/MKDAndroid/Ficheros/'
idDispositivo=$2

while :
do 
case $1 in
    "ini") echo "Subiendo el fichero ini..."
    adb push $localPath'Ini/'$idDispositivo.json $otsFilePath'hmsipplayer.json'
    break
    ;;
esac
done 