CONFIGURACION DE EKS EN WINDOWS

https://docs.aws.amazon.com/es_es/eks/latest/userguide/eks-ug.pdf

1) Creamos una carpeta donde almacenaremos los siguientes archivos (p.e. C:\Kubewin)	

 - https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/windows/amd64/kubectl.exe
 - https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/windows/amd64/aws-iam-authenticator.exe
 
2) Descargamos e instalamos el aws cli

 - https://s3.amazonaws.com/aws-cli/AWSCLI64.msi  (instalador de 64-bits)
 
3) Creamos el archivo config, que tendrá el siguiente contenido

apiVersion: v1
clusters:
- cluster:
    server: https://DC6AE13CDBB96FCE53BFBF2BAE5C6492.yl4.eu-west-1.eks.amazonaws.com 
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN5RENDQWJDZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFWTVJNd0VRWURWUVFERXdwcmRXSmwKY201bGRHVnpNQjRYRFRFNE1Ea3hPREl3TWpjd09Wb1hEVEk0TURreE5USXdNamN3T1Zvd0ZURVRNQkVHQTFVRQpBeE1LYTNWaVpYSnVaWFJsY3pDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBTEdWClpQb3VjT0oxS0ZsK0lZMnBFMG1hMmpiaHFlNVBGMjFpdFp1aWpRTllIa2JHSzJYbWIvalJOUTVyVEJVMktSd0QKLzA4WmZibUU1d1dQU0VocEp4ZnU1c25mWXhzeXB0M2dIRkgrQzRoY3I2dnY3OFJGRUp0bEluVGgwbUQySjZQaQpNajZ5eVl6NFFNTGdTVlhsWFVFSCtaNWxQZlRnQTgveGVqMjE5aWFuMkhpUTRFaWlPay9qS2lRSWRBUitNSnk3CkZvb0lkbEZrL2xmY2VZYVZaZ3J0V3hHYnRUa2piNGtoWWkrN0ZYaWQwdXMyNmh4cHJ3QUZaRGZHTlhQd0M0T0kKR1B5d1JaYnJUMU8vSGV6ZXhPS0c2blRldTBvNDJpK0c1dkM0R0dUVGdyZlhqQ2ZFRjRua2h2MzJ1VldqTVhCNgpPL3JkeWRSdHZZL0JmRG13ZzBVQ0F3RUFBYU1qTUNFd0RnWURWUjBQQVFIL0JBUURBZ0trTUE4R0ExVWRFd0VCCi93UUZNQU1CQWY4d0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFLeWF6OFNTQUQrY2x4TTY5R2MrNGttaXcvdFcKNlMyZUErZ2hmTGVvWDJIYVpLYVNmc2xxN1NLNGt1WFhpYi8yeUJpeFUvZDl4RVkvcFMwMHNQTUg5ZVR6NmZDRwpTbUlGbVdPaUJQeWdDT1YvMzc5SDVKMWZ1OGtKRHgwSkc3SHQwYkRRTlNhU2g5bkszUVEzdTBrM2VrWi83d1hnCndKdVBNdWpuVDB5VllTYlVJNHNUNVBDNDAvVU9aMkFNaEgvb0wzUU9XeDVRZlR1ejFSMGtwbHhkSFQ0Qk1GOTcKN0U1Y1RVcStQd3dCdWVPakhtV0c3MVVzenBHczhIVEI3cEF3enpxRHlGTVVwT2grOHA1R3NKZ09BR1E5MXFicgprUnVXL3NNZWhsN2ZsVGVIZDNaaWtteXR1aGlnQWtxbjRSMzY4eHVDMlBMVnpIM0g2ZExBTzZCRkRTYz0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "spotdynav2_qa"
      #  - "-r"
      #  - "arn:aws:iam::417822714289:role/eksServiceRole"
      # env:
        # - name: AWS_PROFILE
        #   value: "<aws-profile>"
 
 
4) Creamos las variables de entorno necesarias

	4.1) Accedemos a Panel de control > Sistema > Configuración avanzada del sistema > Variables de entorno
	4.2) Agregamos los directorios donde tenemos guardados los archivos del punto 1 al PATH (Para añadir una nueva variable al PATH, introducimos el valor separado con ; (p.e. Valor de la variable: C:\DIR1;D:\DIR2)
	
		- Directorio de awscli (p.e. C:\Program Files\Amazon\AWSCLI)
		- Directorio de kubectl (p.e. C:\Kubewin)
		
	4.3) Agregamos la variable de entorno KUBECONFIG con valor = ruta del archivo config (p.e. C:\Kubewin\config)
	
5) Abrimos un terminal en windows (Windows PowerShell)
6) Ejecutamos el awscli con el comando aws configure
7) Introducimos las credenciales de AWSEKS:

	- AWS Access Key ID
	- AWS Secret Access Key
	- Default region name: eu-west-1
	- Default output format
	
8) Verificamos que el acceso a EKS funciona correctamente con el comando kubectl cluster-info
	
	
	
	
	