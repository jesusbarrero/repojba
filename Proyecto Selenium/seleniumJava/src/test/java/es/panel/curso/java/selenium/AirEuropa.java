package es.panel.curso.java.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AirEuropa {
	
	private WebDriver driver;
	private WebElement a;
	
	public AirEuropa () {
		super();
		this.driver = new ChromeDriver();
		this.driver.get("https://www.aireuropa.com/es/vuelos");
		//this.a = getWebElement(By.id("departure"));
		Utilidades.sleepTimeInSegundos(1);
	}
	
	public void setWebElement(By by) {
		this.a = this.driver.findElement(by);
	}
	
	public WebElement getWebElement() {
		return this.a;
	}
	
	public WebDriver getWebDriver() {
		return this.driver;
	}


}
