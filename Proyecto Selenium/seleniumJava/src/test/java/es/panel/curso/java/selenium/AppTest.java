package es.panel.curso.java.selenium;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.qameta.allure.Epic;
import io.qameta.allure.Feature;


@Epic("Alumno Tests Epic")
@Feature("Super Features")
class AppTest {
	
	private AirEuropa page;
	
	@BeforeEach
	public void initEach() {
		
	}

	@Test
	void testDeparture() {
		
		page = new AirEuropa();
		page.setWebElement(By.id("departure"));
		page.getWebElement().sendKeys("Madrid");
		
		String actURL = page.getWebDriver().getTitle();
		assertEquals(actURL,"Air Europa - Web Oficial. Reserve vuelos baratos online");
		Utilidades.sleepTimeInSegundos(2);
	}
	
	@AfterEach
	public void endEach(){
		page.getWebDriver().quit();
	}
}
