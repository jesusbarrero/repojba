var fs = require('fs');
var { Document } = require('adf-builder');
const doc = new Document();
doc.panel("info")
    .paragraph()
    .strong("Descripcion de la incidencia")
    .text("Pequeña descripcion 1")
    .code("Aqui va el describe del pod");
doc.panel("info")
    .paragraph()
    .strong("Comportamiento esperado")
    .text("Pequeña descripcion 2");
doc.panel("warning")
    .paragraph()
    .strong("Pasos para reproducir")
    .text("1.Paso uno 2.Paso dos")
    .em("¿Reproducible?")
    .text("Si")
doc.panel("error")
    .paragraph()
    .strong("Se adjuntan evidencias");
var reply = doc.toString();

fs.writeFile('adf-out.json', reply, function(err) {
    // If an error occurred, show it and return
    if(err) return console.error(err);
    // Successfully wrote to the file!
});
