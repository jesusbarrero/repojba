Pipeline en jenkins

def suite = env.testSuite
def entorno = env.entorno

node("ejecutorTests"){
    currentBuild.displayName = "Cypress -- ${suite} -- ${entorno}"
    currentBuild.description = "Prueba de ${suite} en el entorno ${entorno} con el usuario ${emailSpotdyna}"
    try{
        stage('Clean workspace'){
            step([$class: 'WsCleanup', cleanWhenFailure: false])
            
        }

        stage('Checkout'){
         checkOut()
        }

        stage('Build'){ 
           bat 'npm install'
        }
        
        
        stage('Parameterize cucumber feature'){
             
        def fichero = "cypress/integration/${suite}.feature"
        def ficheroPlantilla = "cypress/integration/plantillas/${suite}.plantilla"
        
        copyFile(ficheroPlantilla,fichero)
        
        readAndReplace(fichero,'TAG_EMAIL',env.emailSpotdyna)  //email
        readAndReplace(fichero,'TAG_PASSWORD',env.passwordSpotdyna)  //password
        readAndReplace(fichero,'TAG_USERSPOTDYNA',env.userSpotdyna)  //userSpotdyna
        
 
        }

        stage('Tests execution'){
            withEnv(["CYPRESS_ENV=${entorno}"]){
            bat "npx cypress run --env TAGS=@${suite}"
            }
        }    
    } catch (e){
        currentBuild.result = "FAILED"
        throw e
    } finally{
        stage ('Archive Results & Artifacts'){
            try{
                if (currentBuild.result == 'FAILURE'){
                    currentBuild.result = 'UNSTABLE'
                }   
            } catch (e) {
                currentBuild.result = 'FAILURE'
            }
            
            archiveArtifacts artifacts: "cypress/screenshots/${suite}.feature.png, cypress/videos/${suite}.feature.mp4", fingerprint: true, onlyIfSuccessful: false
        }
    
        stage('Publish Cucumber tests results'){
            try{
                step([$class: 'CucumberReportPublisher', jsonReportDirectory: 'cypress/cucumber-json', fileIncludePattern:"*.cucumber.json" ])
            } catch (e){
                echo "El reporte Cucumber no ha podido ser publicado: " + e.getMessage()
            }
        }
    }
        
}


def checkOut() {
    def gitUrl = 'https://bitbucket.org/telefonicaonthespot/cypressauto.git'
    def gitBranch = "master"
    def gitCredentials = 'qa'
    git url: gitUrl ,credentialsId: gitCredentials, branch: gitBranch
}


def readAndReplace(file,stringToFind,stringToReplace) {
    String cadena = readFile(file).replaceAll(stringToFind,stringToReplace)
    writeFile file: file, text: cadena
}

def copyFile(fileToCopy,fileToPaste){
    String cadena = readFile(fileToCopy)
    writeFile file: fileToPaste, text: cadena
}