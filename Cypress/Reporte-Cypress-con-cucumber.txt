Basado en https://stackoverflow.com/questions/60036258/can-i-generate-cucumbur-html-report-in-cypress

Agregar en el fichero package.json la configuración necesaria para el plugin cypress-cucumber-preprocessor

"cypress-cucumber-preprocessor": {
        "nonGlobalStepDefinitions": false,
        "stepDefinitions": "cypress/integration/**/*.js",
        "cucumberJson": {
            "generate": true,
            "outputFolder": "cypress/cucumber-json",
            "filePrefix": "",
            "fileSuffix": ".cucumber"
        }
    } 