#!bin/bash

getJenkinsDockerContainerId(){
    docker ps | grep jenkins | cut -f1 -d" "
}

docker cp $(getJenkinsDockerContainerId):/var/jenkins_home .