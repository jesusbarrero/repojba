/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 798.0, "minX": 0.0, "maxY": 57725.0, "series": [{"data": [[0.0, 798.0], [0.1, 798.0], [0.2, 1074.0], [0.3, 1116.0], [0.4, 1116.0], [0.5, 1120.0], [0.6, 1122.0], [0.7, 1130.0], [0.8, 1130.0], [0.9, 1178.0], [1.0, 1179.0], [1.1, 1180.0], [1.2, 1180.0], [1.3, 1187.0], [1.4, 1194.0], [1.5, 1194.0], [1.6, 1196.0], [1.7, 1377.0], [1.8, 1466.0], [1.9, 1466.0], [2.0, 1525.0], [2.1, 1551.0], [2.2, 1552.0], [2.3, 1552.0], [2.4, 1560.0], [2.5, 1581.0], [2.6, 1594.0], [2.7, 1594.0], [2.8, 1595.0], [2.9, 1600.0], [3.0, 1600.0], [3.1, 1600.0], [3.2, 1600.0], [3.3, 1672.0], [3.4, 1672.0], [3.5, 1686.0], [3.6, 1686.0], [3.7, 1688.0], [3.8, 1688.0], [3.9, 1704.0], [4.0, 1704.0], [4.1, 1704.0], [4.2, 1704.0], [4.3, 1711.0], [4.4, 1717.0], [4.5, 1717.0], [4.6, 1729.0], [4.7, 1745.0], [4.8, 1825.0], [4.9, 1825.0], [5.0, 1830.0], [5.1, 1850.0], [5.2, 1854.0], [5.3, 1854.0], [5.4, 1964.0], [5.5, 1969.0], [5.6, 1969.0], [5.7, 1974.0], [5.8, 1975.0], [5.9, 1983.0], [6.0, 1983.0], [6.1, 1985.0], [6.2, 1996.0], [6.3, 2095.0], [6.4, 2095.0], [6.5, 2105.0], [6.6, 2113.0], [6.7, 2113.0], [6.8, 2127.0], [6.9, 2133.0], [7.0, 2147.0], [7.1, 2147.0], [7.2, 2174.0], [7.3, 2259.0], [7.4, 2305.0], [7.5, 2305.0], [7.6, 2323.0], [7.7, 2338.0], [7.8, 2350.0], [7.9, 2350.0], [8.0, 2356.0], [8.1, 2388.0], [8.2, 2388.0], [8.3, 2400.0], [8.4, 2408.0], [8.5, 2410.0], [8.6, 2410.0], [8.7, 2412.0], [8.8, 2415.0], [8.9, 2419.0], [9.0, 2419.0], [9.1, 2435.0], [9.2, 2503.0], [9.3, 2503.0], [9.4, 2516.0], [9.5, 2522.0], [9.6, 2528.0], [9.7, 2528.0], [9.8, 2530.0], [9.9, 2531.0], [10.0, 2579.0], [10.1, 2579.0], [10.2, 2587.0], [10.3, 2634.0], [10.4, 2931.0], [10.5, 2931.0], [10.6, 2941.0], [10.7, 2941.0], [10.8, 2941.0], [10.9, 2999.0], [11.0, 3031.0], [11.1, 3292.0], [11.2, 3292.0], [11.3, 3302.0], [11.4, 3304.0], [11.5, 3309.0], [11.6, 3309.0], [11.7, 3310.0], [11.8, 3314.0], [11.9, 3314.0], [12.0, 3318.0], [12.1, 3324.0], [12.2, 3326.0], [12.3, 3326.0], [12.4, 3382.0], [12.5, 3385.0], [12.6, 3387.0], [12.7, 3387.0], [12.8, 3405.0], [12.9, 3428.0], [13.0, 3455.0], [13.1, 3455.0], [13.2, 3480.0], [13.3, 3483.0], [13.4, 3483.0], [13.5, 3498.0], [13.6, 3510.0], [13.7, 3511.0], [13.8, 3511.0], [13.9, 3512.0], [14.0, 3516.0], [14.1, 3517.0], [14.2, 3517.0], [14.3, 3519.0], [14.4, 3521.0], [14.5, 3521.0], [14.6, 3524.0], [14.7, 3529.0], [14.8, 3536.0], [14.9, 3536.0], [15.0, 3541.0], [15.1, 3544.0], [15.2, 3546.0], [15.3, 3546.0], [15.4, 3564.0], [15.5, 3616.0], [15.6, 3655.0], [15.7, 3655.0], [15.8, 3704.0], [15.9, 3770.0], [16.0, 3770.0], [16.1, 3777.0], [16.2, 3790.0], [16.3, 3793.0], [16.4, 3793.0], [16.5, 3795.0], [16.6, 3803.0], [16.7, 3804.0], [16.8, 3804.0], [16.9, 3819.0], [17.0, 3825.0], [17.1, 3829.0], [17.2, 3829.0], [17.3, 3849.0], [17.4, 3882.0], [17.5, 3882.0], [17.6, 3889.0], [17.7, 3930.0], [17.8, 3931.0], [17.9, 3931.0], [18.0, 3931.0], [18.1, 3934.0], [18.2, 3934.0], [18.3, 3934.0], [18.4, 3953.0], [18.5, 4074.0], [18.6, 4074.0], [18.7, 4120.0], [18.8, 4166.0], [18.9, 4169.0], [19.0, 4169.0], [19.1, 4198.0], [19.2, 4207.0], [19.3, 4211.0], [19.4, 4211.0], [19.5, 4211.0], [19.6, 4260.0], [19.7, 4274.0], [19.8, 4274.0], [19.9, 4368.0], [20.0, 4370.0], [20.1, 4370.0], [20.2, 4377.0], [20.3, 4415.0], [20.4, 4429.0], [20.5, 4429.0], [20.6, 4433.0], [20.7, 4487.0], [20.8, 4527.0], [20.9, 4527.0], [21.0, 4633.0], [21.1, 4694.0], [21.2, 4694.0], [21.3, 4696.0], [21.4, 4698.0], [21.5, 4733.0], [21.6, 4733.0], [21.7, 4733.0], [21.8, 4769.0], [21.9, 4771.0], [22.0, 4771.0], [22.1, 4775.0], [22.2, 4788.0], [22.3, 4795.0], [22.4, 4795.0], [22.5, 4843.0], [22.6, 4844.0], [22.7, 4844.0], [22.8, 4854.0], [22.9, 4859.0], [23.0, 4869.0], [23.1, 4869.0], [23.2, 4898.0], [23.3, 4922.0], [23.4, 4922.0], [23.5, 4922.0], [23.6, 4923.0], [23.7, 4929.0], [23.8, 4929.0], [23.9, 4929.0], [24.0, 4938.0], [24.1, 4942.0], [24.2, 4942.0], [24.3, 4944.0], [24.4, 5047.0], [24.5, 5092.0], [24.6, 5092.0], [24.7, 5103.0], [24.8, 5106.0], [24.9, 5115.0], [25.0, 5115.0], [25.1, 5140.0], [25.2, 5162.0], [25.3, 5162.0], [25.4, 5191.0], [25.5, 5221.0], [25.6, 5226.0], [25.7, 5226.0], [25.8, 5227.0], [25.9, 5245.0], [26.0, 5249.0], [26.1, 5249.0], [26.2, 5276.0], [26.3, 5277.0], [26.4, 5277.0], [26.5, 5294.0], [26.6, 5346.0], [26.7, 5348.0], [26.8, 5348.0], [26.9, 5357.0], [27.0, 5373.0], [27.1, 5385.0], [27.2, 5385.0], [27.3, 5405.0], [27.4, 5406.0], [27.5, 5413.0], [27.6, 5413.0], [27.7, 5450.0], [27.8, 5456.0], [27.9, 5456.0], [28.0, 5458.0], [28.1, 5462.0], [28.2, 5467.0], [28.3, 5467.0], [28.4, 5479.0], [28.5, 5500.0], [28.6, 5502.0], [28.7, 5502.0], [28.8, 5512.0], [28.9, 5529.0], [29.0, 5529.0], [29.1, 5534.0], [29.2, 5535.0], [29.3, 5541.0], [29.4, 5541.0], [29.5, 5541.0], [29.6, 5548.0], [29.7, 5549.0], [29.8, 5549.0], [29.9, 5551.0], [30.0, 5553.0], [30.1, 5554.0], [30.2, 5554.0], [30.3, 5556.0], [30.4, 5569.0], [30.5, 5569.0], [30.6, 5580.0], [30.7, 5594.0], [30.8, 5597.0], [30.9, 5597.0], [31.0, 5599.0], [31.1, 5600.0], [31.2, 5630.0], [31.3, 5630.0], [31.4, 5632.0], [31.5, 5634.0], [31.6, 5634.0], [31.7, 5636.0], [31.8, 5639.0], [31.9, 5639.0], [32.0, 5639.0], [32.1, 5640.0], [32.2, 5641.0], [32.3, 5644.0], [32.4, 5644.0], [32.5, 5647.0], [32.6, 5647.0], [32.7, 5648.0], [32.8, 5648.0], [32.9, 5653.0], [33.0, 5653.0], [33.1, 5653.0], [33.2, 5657.0], [33.3, 5657.0], [33.4, 5659.0], [33.5, 5659.0], [33.6, 5660.0], [33.7, 5662.0], [33.8, 5669.0], [33.9, 5669.0], [34.0, 5670.0], [34.1, 5672.0], [34.2, 5677.0], [34.3, 5677.0], [34.4, 5678.0], [34.5, 5679.0], [34.6, 5679.0], [34.7, 5684.0], [34.8, 5684.0], [34.9, 5684.0], [35.0, 5684.0], [35.1, 5684.0], [35.2, 5685.0], [35.3, 5686.0], [35.4, 5686.0], [35.5, 5694.0], [35.6, 5694.0], [35.7, 5694.0], [35.8, 5695.0], [35.9, 5698.0], [36.0, 5700.0], [36.1, 5700.0], [36.2, 5701.0], [36.3, 5704.0], [36.4, 5705.0], [36.5, 5705.0], [36.6, 5711.0], [36.7, 5711.0], [36.8, 5712.0], [36.9, 5712.0], [37.0, 5713.0], [37.1, 5714.0], [37.2, 5714.0], [37.3, 5717.0], [37.4, 5720.0], [37.5, 5721.0], [37.6, 5721.0], [37.7, 5725.0], [37.8, 5725.0], [37.9, 5726.0], [38.0, 5726.0], [38.1, 5735.0], [38.2, 5738.0], [38.3, 5738.0], [38.4, 5739.0], [38.5, 5743.0], [38.6, 5745.0], [38.7, 5745.0], [38.8, 5745.0], [38.9, 5748.0], [39.0, 5751.0], [39.1, 5751.0], [39.2, 5755.0], [39.3, 5760.0], [39.4, 5767.0], [39.5, 5767.0], [39.6, 5771.0], [39.7, 5772.0], [39.8, 5772.0], [39.9, 5773.0], [40.0, 5774.0], [40.1, 5775.0], [40.2, 5775.0], [40.3, 5779.0], [40.4, 5785.0], [40.5, 5787.0], [40.6, 5787.0], [40.7, 5790.0], [40.8, 5793.0], [40.9, 5793.0], [41.0, 5796.0], [41.1, 5807.0], [41.2, 5813.0], [41.3, 5813.0], [41.4, 5826.0], [41.5, 5833.0], [41.6, 5846.0], [41.7, 5846.0], [41.8, 5856.0], [41.9, 5856.0], [42.0, 5857.0], [42.1, 5857.0], [42.2, 5859.0], [42.3, 5862.0], [42.4, 5862.0], [42.5, 5863.0], [42.6, 5863.0], [42.7, 5864.0], [42.8, 5864.0], [42.9, 5865.0], [43.0, 5866.0], [43.1, 5867.0], [43.2, 5867.0], [43.3, 5868.0], [43.4, 5868.0], [43.5, 5868.0], [43.6, 5868.0], [43.7, 5869.0], [43.8, 5870.0], [43.9, 5870.0], [44.0, 5871.0], [44.1, 5871.0], [44.2, 5871.0], [44.3, 5871.0], [44.4, 5873.0], [44.5, 5873.0], [44.6, 5874.0], [44.7, 5874.0], [44.8, 5874.0], [44.9, 5875.0], [45.0, 5875.0], [45.1, 5876.0], [45.2, 5877.0], [45.3, 5877.0], [45.4, 5877.0], [45.5, 5878.0], [45.6, 5878.0], [45.7, 5878.0], [45.8, 5878.0], [45.9, 5878.0], [46.0, 5878.0], [46.1, 5878.0], [46.2, 5879.0], [46.3, 5880.0], [46.4, 5880.0], [46.5, 5880.0], [46.6, 5880.0], [46.7, 5880.0], [46.8, 5881.0], [46.9, 5881.0], [47.0, 5881.0], [47.1, 5882.0], [47.2, 5882.0], [47.3, 5882.0], [47.4, 5882.0], [47.5, 5882.0], [47.6, 5882.0], [47.7, 5882.0], [47.8, 5883.0], [47.9, 5883.0], [48.0, 5883.0], [48.1, 5884.0], [48.2, 5884.0], [48.3, 5885.0], [48.4, 5885.0], [48.5, 5886.0], [48.6, 5886.0], [48.7, 5886.0], [48.8, 5886.0], [48.9, 5887.0], [49.0, 5888.0], [49.1, 5888.0], [49.2, 5888.0], [49.3, 5889.0], [49.4, 5889.0], [49.5, 5889.0], [49.6, 5889.0], [49.7, 5892.0], [49.8, 5894.0], [49.9, 5894.0], [50.0, 5894.0], [50.1, 5894.0], [50.2, 5894.0], [50.3, 5895.0], [50.4, 5896.0], [50.5, 5898.0], [50.6, 5898.0], [50.7, 5901.0], [50.8, 5901.0], [50.9, 5902.0], [51.0, 5902.0], [51.1, 5902.0], [51.2, 5903.0], [51.3, 5903.0], [51.4, 5903.0], [51.5, 5905.0], [51.6, 5907.0], [51.7, 5907.0], [51.8, 5907.0], [51.9, 5908.0], [52.0, 5910.0], [52.1, 5910.0], [52.2, 5913.0], [52.3, 5914.0], [52.4, 5915.0], [52.5, 5915.0], [52.6, 5916.0], [52.7, 5916.0], [52.8, 5916.0], [52.9, 5917.0], [53.0, 5917.0], [53.1, 5917.0], [53.2, 5917.0], [53.3, 5918.0], [53.4, 5918.0], [53.5, 5918.0], [53.6, 5918.0], [53.7, 5918.0], [53.8, 5920.0], [53.9, 5923.0], [54.0, 5923.0], [54.1, 5924.0], [54.2, 5925.0], [54.3, 5925.0], [54.4, 5927.0], [54.5, 5927.0], [54.6, 5927.0], [54.7, 5927.0], [54.8, 5928.0], [54.9, 5928.0], [55.0, 5929.0], [55.1, 5929.0], [55.2, 5929.0], [55.3, 5932.0], [55.4, 5932.0], [55.5, 5933.0], [55.6, 5933.0], [55.7, 5934.0], [55.8, 5934.0], [55.9, 5935.0], [56.0, 5936.0], [56.1, 5940.0], [56.2, 5940.0], [56.3, 5940.0], [56.4, 5941.0], [56.5, 5947.0], [56.6, 5947.0], [56.7, 5949.0], [56.8, 5951.0], [56.9, 5951.0], [57.0, 5952.0], [57.1, 5953.0], [57.2, 5953.0], [57.3, 5953.0], [57.4, 5956.0], [57.5, 5957.0], [57.6, 5958.0], [57.7, 5958.0], [57.8, 5958.0], [57.9, 5964.0], [58.0, 5964.0], [58.1, 5967.0], [58.2, 5971.0], [58.3, 5977.0], [58.4, 5977.0], [58.5, 5980.0], [58.6, 5980.0], [58.7, 5981.0], [58.8, 5981.0], [58.9, 5993.0], [59.0, 5997.0], [59.1, 6006.0], [59.2, 6006.0], [59.3, 6006.0], [59.4, 6008.0], [59.5, 6008.0], [59.6, 6009.0], [59.7, 6010.0], [59.8, 6021.0], [59.9, 6021.0], [60.0, 6021.0], [60.1, 6026.0], [60.2, 6026.0], [60.3, 6026.0], [60.4, 6027.0], [60.5, 6027.0], [60.6, 6027.0], [60.7, 6027.0], [60.8, 6027.0], [60.9, 6028.0], [61.0, 6028.0], [61.1, 6029.0], [61.2, 6029.0], [61.3, 6030.0], [61.4, 6030.0], [61.5, 6030.0], [61.6, 6031.0], [61.7, 6032.0], [61.8, 6032.0], [61.9, 6033.0], [62.0, 6033.0], [62.1, 6033.0], [62.2, 6035.0], [62.3, 6035.0], [62.4, 6036.0], [62.5, 6036.0], [62.6, 6038.0], [62.7, 6039.0], [62.8, 6040.0], [62.9, 6040.0], [63.0, 6042.0], [63.1, 6042.0], [63.2, 6042.0], [63.3, 6043.0], [63.4, 6045.0], [63.5, 6045.0], [63.6, 6045.0], [63.7, 6045.0], [63.8, 6046.0], [63.9, 6047.0], [64.0, 6047.0], [64.1, 6048.0], [64.2, 6048.0], [64.3, 6049.0], [64.4, 6049.0], [64.5, 6049.0], [64.6, 6051.0], [64.7, 6051.0], [64.8, 6052.0], [64.9, 6052.0], [65.0, 6053.0], [65.1, 6053.0], [65.2, 6054.0], [65.3, 6056.0], [65.4, 6060.0], [65.5, 6060.0], [65.6, 6060.0], [65.7, 6060.0], [65.8, 6060.0], [65.9, 6061.0], [66.0, 6061.0], [66.1, 6063.0], [66.2, 6063.0], [66.3, 6068.0], [66.4, 6070.0], [66.5, 6096.0], [66.6, 6096.0], [66.7, 6102.0], [66.8, 6104.0], [66.9, 6105.0], [67.0, 6105.0], [67.1, 6108.0], [67.2, 6109.0], [67.3, 6109.0], [67.4, 6112.0], [67.5, 6113.0], [67.6, 6114.0], [67.7, 6114.0], [67.8, 6116.0], [67.9, 6116.0], [68.0, 6118.0], [68.1, 6118.0], [68.2, 6119.0], [68.3, 6120.0], [68.4, 6125.0], [68.5, 6125.0], [68.6, 6128.0], [68.7, 6134.0], [68.8, 6134.0], [68.9, 6136.0], [69.0, 6143.0], [69.1, 6143.0], [69.2, 6143.0], [69.3, 6145.0], [69.4, 6147.0], [69.5, 6151.0], [69.6, 6151.0], [69.7, 6152.0], [69.8, 6152.0], [69.9, 6152.0], [70.0, 6154.0], [70.1, 6158.0], [70.2, 6161.0], [70.3, 6161.0], [70.4, 6163.0], [70.5, 6163.0], [70.6, 6165.0], [70.7, 6165.0], [70.8, 6168.0], [70.9, 6171.0], [71.0, 6174.0], [71.1, 6174.0], [71.2, 6191.0], [71.3, 6200.0], [71.4, 6200.0], [71.5, 6204.0], [71.6, 6205.0], [71.7, 6208.0], [71.8, 6208.0], [71.9, 6208.0], [72.0, 6211.0], [72.1, 6213.0], [72.2, 6213.0], [72.3, 6215.0], [72.4, 6216.0], [72.5, 6216.0], [72.6, 6219.0], [72.7, 6219.0], [72.8, 6220.0], [72.9, 6220.0], [73.0, 6224.0], [73.1, 6224.0], [73.2, 6226.0], [73.3, 6226.0], [73.4, 6231.0], [73.5, 6232.0], [73.6, 6233.0], [73.7, 6233.0], [73.8, 6239.0], [73.9, 6241.0], [74.0, 6241.0], [74.1, 6242.0], [74.2, 6247.0], [74.3, 6251.0], [74.4, 6251.0], [74.5, 6255.0], [74.6, 6256.0], [74.7, 6264.0], [74.8, 6264.0], [74.9, 6266.0], [75.0, 6266.0], [75.1, 6266.0], [75.2, 6268.0], [75.3, 6279.0], [75.4, 6281.0], [75.5, 6281.0], [75.6, 6288.0], [75.7, 6291.0], [75.8, 6294.0], [75.9, 6294.0], [76.0, 6296.0], [76.1, 6299.0], [76.2, 6300.0], [76.3, 6300.0], [76.4, 6302.0], [76.5, 6307.0], [76.6, 6307.0], [76.7, 6307.0], [76.8, 6309.0], [76.9, 6310.0], [77.0, 6310.0], [77.1, 6315.0], [77.2, 6316.0], [77.3, 6316.0], [77.4, 6316.0], [77.5, 6316.0], [77.6, 6321.0], [77.7, 6321.0], [77.8, 6323.0], [77.9, 6324.0], [78.0, 6325.0], [78.1, 6325.0], [78.2, 6326.0], [78.3, 6328.0], [78.4, 6340.0], [78.5, 6340.0], [78.6, 6342.0], [78.7, 6349.0], [78.8, 6351.0], [78.9, 6351.0], [79.0, 6352.0], [79.1, 6356.0], [79.2, 6356.0], [79.3, 6362.0], [79.4, 6366.0], [79.5, 6370.0], [79.6, 6370.0], [79.7, 6377.0], [79.8, 6387.0], [79.9, 6390.0], [80.0, 6390.0], [80.1, 6392.0], [80.2, 6395.0], [80.3, 6395.0], [80.4, 6396.0], [80.5, 6398.0], [80.6, 6402.0], [80.7, 6402.0], [80.8, 6407.0], [80.9, 6407.0], [81.0, 6408.0], [81.1, 6408.0], [81.2, 6409.0], [81.3, 6413.0], [81.4, 6422.0], [81.5, 6422.0], [81.6, 6433.0], [81.7, 6436.0], [81.8, 6436.0], [81.9, 6440.0], [82.0, 6440.0], [82.1, 6441.0], [82.2, 6441.0], [82.3, 6447.0], [82.4, 6449.0], [82.5, 6450.0], [82.6, 6450.0], [82.7, 6454.0], [82.8, 6457.0], [82.9, 6457.0], [83.0, 6457.0], [83.1, 6458.0], [83.2, 6468.0], [83.3, 6468.0], [83.4, 6469.0], [83.5, 6470.0], [83.6, 6484.0], [83.7, 6484.0], [83.8, 6491.0], [83.9, 6494.0], [84.0, 6495.0], [84.1, 6495.0], [84.2, 6500.0], [84.3, 6501.0], [84.4, 6501.0], [84.5, 6503.0], [84.6, 6515.0], [84.7, 6547.0], [84.8, 6547.0], [84.9, 6554.0], [85.0, 6562.0], [85.1, 6564.0], [85.2, 6564.0], [85.3, 6573.0], [85.4, 6576.0], [85.5, 6595.0], [85.6, 6595.0], [85.7, 6613.0], [85.8, 6614.0], [85.9, 6614.0], [86.0, 6615.0], [86.1, 6616.0], [86.2, 6618.0], [86.3, 6618.0], [86.4, 6619.0], [86.5, 6623.0], [86.6, 6630.0], [86.7, 6630.0], [86.8, 6633.0], [86.9, 6635.0], [87.0, 6635.0], [87.1, 6636.0], [87.2, 6638.0], [87.3, 6656.0], [87.4, 6656.0], [87.5, 6657.0], [87.6, 6682.0], [87.7, 6756.0], [87.8, 6756.0], [87.9, 6801.0], [88.0, 6855.0], [88.1, 6875.0], [88.2, 6875.0], [88.3, 6888.0], [88.4, 6889.0], [88.5, 6889.0], [88.6, 6925.0], [88.7, 6936.0], [88.8, 6939.0], [88.9, 6939.0], [89.0, 6965.0], [89.1, 6999.0], [89.2, 7009.0], [89.3, 7009.0], [89.4, 7010.0], [89.5, 7017.0], [89.6, 7017.0], [89.7, 7019.0], [89.8, 7039.0], [89.9, 7117.0], [90.0, 7117.0], [90.1, 7165.0], [90.2, 7212.0], [90.3, 7240.0], [90.4, 7240.0], [90.5, 7304.0], [90.6, 7313.0], [90.7, 7368.0], [90.8, 7368.0], [90.9, 7377.0], [91.0, 7428.0], [91.1, 7428.0], [91.2, 7526.0], [91.3, 7534.0], [91.4, 7550.0], [91.5, 7550.0], [91.6, 7559.0], [91.7, 7592.0], [91.8, 7631.0], [91.9, 7631.0], [92.0, 7646.0], [92.1, 7669.0], [92.2, 7669.0], [92.3, 7681.0], [92.4, 7739.0], [92.5, 7739.0], [92.6, 7739.0], [92.7, 7831.0], [92.8, 8010.0], [92.9, 8022.0], [93.0, 8022.0], [93.1, 8070.0], [93.2, 8458.0], [93.3, 8696.0], [93.4, 8696.0], [93.5, 8727.0], [93.6, 9091.0], [93.7, 9091.0], [93.8, 9100.0], [93.9, 9104.0], [94.0, 9110.0], [94.1, 9110.0], [94.2, 9118.0], [94.3, 9316.0], [94.4, 10065.0], [94.5, 10065.0], [94.6, 10418.0], [94.7, 10631.0], [94.8, 10631.0], [94.9, 10655.0], [95.0, 10940.0], [95.1, 10948.0], [95.2, 10948.0], [95.3, 10951.0], [95.4, 10958.0], [95.5, 10988.0], [95.6, 10988.0], [95.7, 11059.0], [95.8, 11081.0], [95.9, 11083.0], [96.0, 11083.0], [96.1, 11083.0], [96.2, 11087.0], [96.3, 11087.0], [96.4, 11116.0], [96.5, 11208.0], [96.6, 11245.0], [96.7, 11245.0], [96.8, 11248.0], [96.9, 11252.0], [97.0, 11252.0], [97.1, 11252.0], [97.2, 11263.0], [97.3, 11263.0], [97.4, 11263.0], [97.5, 11270.0], [97.6, 11399.0], [97.7, 11410.0], [97.8, 11410.0], [97.9, 11420.0], [98.0, 11425.0], [98.1, 11446.0], [98.2, 11446.0], [98.3, 11578.0], [98.4, 11698.0], [98.5, 11729.0], [98.6, 11729.0], [98.7, 11754.0], [98.8, 11772.0], [98.9, 11772.0], [99.0, 11789.0], [99.1, 11805.0], [99.2, 11807.0], [99.3, 11807.0], [99.4, 11824.0], [99.5, 11848.0], [99.6, 15096.0], [99.7, 15096.0], [99.8, 15620.0], [99.9, 57725.0], [100.0, 57725.0]], "isOverall": false, "label": "Login", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 700.0, "maxY": 70.0, "series": [{"data": [[700.0, 1.0], [1000.0, 1.0], [1100.0, 10.0], [1300.0, 1.0], [1400.0, 1.0], [1500.0, 7.0], [1600.0, 7.0], [1700.0, 7.0], [1800.0, 4.0], [1900.0, 7.0], [2000.0, 1.0], [2100.0, 6.0], [2300.0, 6.0], [2200.0, 1.0], [2400.0, 7.0], [2500.0, 8.0], [2600.0, 1.0], [2900.0, 4.0], [3000.0, 1.0], [3300.0, 11.0], [3200.0, 1.0], [3400.0, 6.0], [3500.0, 14.0], [3700.0, 6.0], [3600.0, 2.0], [3800.0, 8.0], [3900.0, 6.0], [4000.0, 1.0], [4200.0, 5.0], [4100.0, 4.0], [4300.0, 3.0], [4400.0, 4.0], [4600.0, 4.0], [4500.0, 1.0], [4700.0, 7.0], [4800.0, 6.0], [5100.0, 6.0], [5000.0, 2.0], [4900.0, 8.0], [5200.0, 8.0], [5300.0, 5.0], [5400.0, 9.0], [5500.0, 19.0], [5600.0, 36.0], [5700.0, 37.0], [5800.0, 70.0], [6100.0, 34.0], [5900.0, 62.0], [6000.0, 55.0], [6200.0, 36.0], [6300.0, 32.0], [6600.0, 15.0], [6500.0, 11.0], [6400.0, 26.0], [6800.0, 5.0], [6900.0, 5.0], [6700.0, 1.0], [7000.0, 5.0], [7100.0, 2.0], [7300.0, 4.0], [7200.0, 2.0], [7400.0, 1.0], [7600.0, 4.0], [7500.0, 5.0], [7700.0, 2.0], [7800.0, 1.0], [8000.0, 3.0], [8400.0, 1.0], [8600.0, 1.0], [8700.0, 1.0], [9000.0, 1.0], [9100.0, 4.0], [9300.0, 1.0], [10000.0, 1.0], [10400.0, 1.0], [10600.0, 2.0], [11000.0, 5.0], [11200.0, 8.0], [10900.0, 5.0], [11100.0, 1.0], [11500.0, 1.0], [11600.0, 1.0], [11700.0, 4.0], [11400.0, 4.0], [11300.0, 1.0], [11800.0, 4.0], [15000.0, 1.0], [15600.0, 1.0], [57700.0, 1.0]], "isOverall": false, "label": "Login", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 57700.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 14.0, "minX": 1.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 717.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 14.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 717.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 2.5, "minX": 1.55084058E12, "maxY": 45.98250000000002, "series": [{"data": [[1.55084064E12, 45.98250000000002], [1.5508407E12, 2.5], [1.55084058E12, 38.48929663608561]], "isOverall": false, "label": "jp@gc - Ultimate Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5508407E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 1536.25, "minX": 1.0, "maxY": 57725.0, "series": [{"data": [[2.0, 15096.0], [3.0, 15620.0], [4.0, 11772.0], [5.0, 10631.0], [6.0, 6450.0], [7.0, 3226.2857142857147], [8.0, 2483.5], [9.0, 1611.4285714285713], [10.0, 3326.0], [11.0, 1536.25], [12.0, 1744.3333333333333], [13.0, 1745.0], [14.0, 2072.0], [15.0, 4943.0], [16.0, 2087.923076923077], [17.0, 2818.666666666667], [18.0, 4416.0], [19.0, 2637.1], [20.0, 2354.0], [21.0, 3026.0], [22.0, 3481.6666666666665], [23.0, 2875.8571428571427], [24.0, 3387.0], [25.0, 2352.8333333333335], [26.0, 3308.0], [27.0, 8727.0], [29.0, 3561.0769230769233], [30.0, 4219.727272727273], [31.0, 4956.8], [32.0, 5445.666666666667], [33.0, 5164.6], [34.0, 7346.666666666667], [35.0, 5188.666666666667], [36.0, 4753.285714285715], [37.0, 4935.833333333334], [38.0, 5226.875000000001], [39.0, 5903.25], [40.0, 5303.0], [41.0, 5212.444444444444], [43.0, 5360.25], [42.0, 5906.5], [44.0, 5175.583333333333], [45.0, 5620.2], [46.0, 7035.75], [47.0, 6310.545454545455], [48.0, 5888.4], [49.0, 7188.909090909091], [50.0, 6463.513918629552], [1.0, 57725.0]], "isOverall": false, "label": "Login", "isController": false}, {"data": [[42.39261285909713, 5754.711354309165]], "isOverall": false, "label": "Login-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 50.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 20.4, "minX": 1.55084058E12, "maxY": 20466.216666666667, "series": [{"data": [[1.55084064E12, 20466.216666666667], [1.5508407E12, 204.66666666666666], [1.55084058E12, 16731.5]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.55084064E12, 2040.0], [1.5508407E12, 20.4], [1.55084058E12, 1667.7]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5508407E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 4778.302752293581, "minX": 1.55084058E12, "maxY": 25053.25, "series": [{"data": [[1.55084064E12, 6359.940000000002], [1.5508407E12, 25053.25], [1.55084058E12, 4778.302752293581]], "isOverall": false, "label": "Login", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5508407E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 4778.22935779816, "minX": 1.55084058E12, "maxY": 25053.25, "series": [{"data": [[1.55084064E12, 6359.877499999999], [1.5508407E12, 25053.25], [1.55084058E12, 4778.22935779816]], "isOverall": false, "label": "Login", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5508407E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 139.0, "minX": 1.55084058E12, "maxY": 216.60500000000008, "series": [{"data": [[1.55084064E12, 216.60500000000008], [1.5508407E12, 139.0], [1.55084058E12, 179.7920489296636]], "isOverall": false, "label": "Login", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5508407E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 798.0, "minX": 1.55084058E12, "maxY": 57725.0, "series": [{"data": [[1.55084064E12, 11848.0], [1.5508407E12, 57725.0], [1.55084058E12, 8458.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.55084064E12, 1729.0], [1.5508407E12, 11772.0], [1.55084058E12, 798.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.55084064E12, 7017.4], [1.5508407E12, 7155.400000000003], [1.55084058E12, 6764.999999999999]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.55084064E12, 11720.320000000002], [1.5508407E12, 11799.88], [1.55084058E12, 7959.879999999995]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.55084064E12, 10276.800000000008], [1.5508407E12, 10943.2], [1.55084058E12, 7228.799999999999]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5508407E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 5888.5, "minX": 1000.0, "maxY": 5956.5, "series": [{"data": [[3000.0, 5956.5], [1000.0, 5896.0], [2000.0, 5888.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1, "maxX": 3000.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.create();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 5888.5, "minX": 1000.0, "maxY": 5956.5, "series": [{"data": [[3000.0, 5956.5], [1000.0, 5896.0], [2000.0, 5888.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1, "maxX": 3000.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 5.9, "minX": 1.55084058E12, "maxY": 6.283333333333333, "series": [{"data": [[1.55084064E12, 5.9], [1.55084058E12, 6.283333333333333]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.55084064E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.06666666666666667, "minX": 1.55084058E12, "maxY": 6.666666666666667, "series": [{"data": [[1.55084064E12, 6.666666666666667], [1.5508407E12, 0.06666666666666667], [1.55084058E12, 5.45]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.5508407E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.06666666666666667, "minX": 1.55084058E12, "maxY": 6.666666666666667, "series": [{"data": [[1.55084064E12, 6.666666666666667], [1.5508407E12, 0.06666666666666667], [1.55084058E12, 5.45]], "isOverall": false, "label": "Login-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5508407E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.06666666666666667, "minX": 1.55084058E12, "maxY": 6.666666666666667, "series": [{"data": [[1.55084064E12, 6.666666666666667], [1.5508407E12, 0.06666666666666667], [1.55084058E12, 5.45]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.5508407E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 3600000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "responseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    choiceContainer.find("label").each(function(){
        this.style.color = color;
    });
}

