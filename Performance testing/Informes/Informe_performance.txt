Buenas,

Os paso un resumen de las pruebas de performance que hemos realizado. El diseño de las mismas se ha definido para tratar de alcanzar los objetivos definidos en https://onthespot.atlassian.net/browse/SPOTDYNA-2318 . Son solo pruebas de ejemplo y el diseño definitivo lo podemos ver en los próximos dias

1. Login (max 100 usuarios concurrentes)

Login.png 

https://a.blazemeter.com/app/?public-token=yMjr2pFKL5375qTzRTpt6dFHqzxU2wMGIshQQWkqmjkO4YCMOs#/accounts/-1/workspaces/-1/projects/-1/sessions/r-ext-5c7ff6d19f6fc/summary/timeline

2. Logs

Logs.png (max 60 usuarios concurrentes)

https://a.blazemeter.com/app/?public-token=DfXNdno2Q7lMxdS9JIehHojWU0MGegFEc0bZha0ntEv5n0G7ZB#/accounts/-1/workspaces/-1/projects/-1/sessions/r-ext-5c7ff7b8c7691/summary/timeline

3. permisos (max 100 usuarios concurrentes)

Permisos.png 

https://a.blazemeter.com/app/?public-token=E6z8Exx71aGnlNZEKx0Ig3qUMIYQyBKbYyyJvqGXxt9DeWQBy6#/accounts/-1/workspaces/-1/projects/-1/sessions/r-ext-5c7ff8e702310/summary/timeline

4. Acciones (50.000 peticiones por un solo hilo) - No se puede ejecutar la prueba :(

Acciones.png

Thread Name: Thread Group 1-1
Sample Start: 2019-03-06 17:54:17 CET
Load time: 3505
Connect Time: 3056
Latency: 3505
Size in bytes: 613
Sent bytes:300933
Headers size in bytes: 0
Body size in bytes: 0
Sample Count: 1
Error Count: 1
Data type ("text"|"bin"|""): text
Response code: 413
Response message: Payload Too Large


SampleResult fields:
ContentType: 
DataEncoding: null

Log del microservicio acciones:

[2019-03-06T16:57:18.318Z][ACCIONES][ERROR][logs.js][request entity too large]


5. Beacon-mensajes (max. 1000 usuarios concurrentes)

beacons-mensajes.png

https://a.blazemeter.com/app/?public-token=uCdW7nTQzClxXLH7QS1z4VEbxkTxSG6vFQvLld3LyyQLe0WuXE#/accounts/-1/workspaces/-1/projects/-1/sessions/r-ext-5c7ffa922e57c/summary/timeline

Un saludo
