************************************

Unmanaged services

EC2
EMR
Elastic Beanstalk
OpsWork

- Full control
- Root level access
- Support any application

************************************

Managed services

S3
DynamoDB
RDS
NAT Gateway

- Dont have to worry about backups or patch
- More secure
- Easier to manage
- High availability

************************************