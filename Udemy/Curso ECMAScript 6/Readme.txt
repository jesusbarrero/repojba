1. Install nodejs
2. Install yarn -> npm install -g yarn

https://yarnpkg.com/getting-started/install

3. git clone https://github.com/djdjalas/es6-course.git

4. install atom text editor
5. cd es6-course
6. yarn
7. To run the server -> yarn run devserver 

	And the server will start on localhost:3000 (you can modify the port on webpack.config.js, line 58
	
8. Open the es6-course folder with atom text editor

9. type log("hello world") into js/index.html file for that to apear in the localhost:3000 webpage

	You can copy-paste whatever code example that you want into index.html (generators, functions, etc..) and it will appear on the website