//const helpers = require("./helpers");
//console.log("PROCESS: ", process);
// var let const

// const {sum,add} = require("./helpers")
// const http = require('http')

// const server = http.createServer((req,res) => {
//     res.end("hello world from nodeJS - updated");
// })

// server.listen(3002);

// const total = helpers.sum(10,22);

// console.log("TOTAL: ", total)

const fs = require('fs')
const filename = "target.txt"
// fs.watch(filename,() => console.log(`file changed`))

const data = fs.readFileSync(filename);
console.log(data.toString());

const errHandler = err => console.log(err);

const dataHandler = data => console.log(data.toString())

fs.readFile(filename,(err,data) => {
    if(err){
        errHandler(err)
    }
    dataHandler(data)
})

console.log('Node JS asynchronous programming... ?')