// Aqui definimos a que funcion del controlador se invoca al acceder a una determinada ruta
// Utilizamos la funcion Router de la libreria express como middleware utilizado por nuestro servidor
const express = require('express')
const {getPosts, createPost} = require('../controllers/post')
const validator = require('../validator/index')

const router = express.Router()

router.get('/',getPosts)
router.post('/post', validator.validations, validator.createPostValidator, createPost)

module.exports = router;

// module.exports = {
//     getPosts
// }