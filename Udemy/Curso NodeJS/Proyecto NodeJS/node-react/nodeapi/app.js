const express = require('express')
const app = express()
const morgan = require('morgan')
const dotenv = require('dotenv');
const mongoose = require('mongoose')
const bodyParser = require('body-parser')


dotenv.config()

//db

mongoose.connect(process.env.MONGO_URI, {useNewUrlParser:true})
.then(()=> {console.log('Connection established successfully')})

mongoose.connection.on('error', err => {console.log(`DB connection error: ${err.message}`)})
// bring in routes

const postRoutes = require('./routes/post');
const validator = require('./validator/index')
//const {getPosts} = require('./routes/post') //utilizamos esta nomenclatura para importar un 
// metodo concreto de la clase requerida

// middleware

// Creamos una funcion callback como middleware personalizado. 
// Esta funcion se ejecuta siempre al realizar una peticion al servidor
// Tenemos que pasarle los parametros req,res,next y ejecutar la funcion next()
// para que permita continuar ejecutandose al servidor, sino no finaliza el callback
// const myOwnMiddleware = (req,res,next) => {
//     console.log("middleware applied!!");
//     next();
// }

app.use(morgan("dev"));
// Asignamos el callback myOwnMiddleware a nuestro servidor
//app.use(myOwnMiddleware);
app.use(bodyParser.json());
app.use("/",postRoutes);

//app.get("/", getPosts)

const port = process.env.SERVER_PORT || 8088;
app.listen(port, () => console.log(`A Node JS API is listening on port: ${port}`));