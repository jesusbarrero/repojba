// Aqui se definen las funciones a ejecutar al acceder a una determinada ruta

const Post = require('../models/post')

exports.getPosts = (req,res) => {
    const posts = Post.find()
    .select("_id title body")
    .then((posts) => {
        // res.status(200) is used by default for successfull requests
        res.json({
            posts  // we can return posts and automatically the name of the object will be the key of the json object
        })
    })
    .catch(err =>{
        res.status(400).json({
            error: err
        })
    })
} 

exports.createPost = (req,res) => {
    const post = new Post(req.body)
    //console.log("CREATING POST:",req.body);
    // post.save((err,result) => {
    //     if(err){
    //         return res.status(400).json({
    //             error: err
    //         })
    //     }
    //     res.status(200).json({
    //         post: result 
    //     })
    // });

    post.save((err,result) => {
        res.status(200).json({
            post: result
        })
    });
}

