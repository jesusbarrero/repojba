const valid = require('express-validator');
const { check, body, validationResult} = require('express-validator');

exports.validations = [
    check('title',"Write a title").notEmpty(),
    check('title',"Title must be between 4 and 150 characters").isLength({
        min: 4,
        max: 150
    })
]

 exports.createPostValidator = (req,res,next) => {
    //title
  const result = validationResult(req);
  if (!result.isEmpty()) {
      return res.status(422).json({
          errors: result.array()
      })
  }
  //proceed to the next middleware
    next();
}

